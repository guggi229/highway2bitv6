/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.scheduletask;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ch.highway2bit.model.PorterTerminal;
import ch.highway2bit.service.TerminalService;

@Component
public class NFCReaderTask {

	private PorterTerminal porterTerminal = new PorterTerminal();

	@Autowired
	private TerminalService terminalService;

	/**
	 * Führt alle x [ms] den Task aus und liest die Daten beim NFC Reader.
	 * 
	 */
	@Scheduled(fixedRate = 500)
	public void checkReader(){
		porterTerminal.readReceiver(terminalService);

	}

}
