/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model;

import java.net.SocketException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.highway2bit.entity.CookieMacMapper;

import ch.highway2bit.terminal.TerminalTemplate;
import ch.highway2bit.utils.MacReader;

/**
 * Diese Klasse ist für das Lesen des NFC-Reader zuständig.
 * @author guggi229
 *
 */
public class PorterTerminal extends TerminalTemplate{
	private static final Logger LOG = LoggerFactory.getLogger(PorterTerminal.class);

	private MacReader macReader;

	private Optional<CookieMacMapper> cookieMacMapperOpt;
	private CookieMacMapper cookieMacMapper;

	/**
	 * Diese Methode erhält die Daten vom NFC-Reader und führt den "Job" dafür aus.
	 * 
	 * Hier wird die MAC Adresse gelesen. Die Daten, die am NFC Reader liegen, werden mit der MAC-Adresse und Cookie ID zusammen in die DB gespeichert.
	 * 
	 * Somit kann mit dieser Cookie ID von überall her die Daten am NFC-Reader gelesen werden.
	 * 
	 */
	@Override
	protected void terminalJob(byte[] answer) {
		macReader = new MacReader();

		try {
			LOG.info("Gefundene MAC-Adresse: {}",macReader.getMacAddess());
			cookieMacMapperOpt = terminalService.cookieMacMapperfindByMac(macReader.getMacAddess());

			if (cookieMacMapperOpt.isPresent()){
				cookieMacMapper=cookieMacMapperOpt.get();
				cookieMacMapper.setNfcId(bytesToHex(answer));
				terminalService.saveNfcId(cookieMacMapper);
			}
			else{
				LOG.error("Kein Mapping mit dieser Mac Adresse!");
				CookieMacMapper cookieMacMapper = new CookieMacMapper();
				cookieMacMapper.setMac(macReader.getMacAddess());
				terminalService.saveNfcId(cookieMacMapper);
			}



		} catch (SocketException e) {
			LOG.error("Unbekannter Fehler");
			System.exit(0);
		}

	}

	/**
	 * Was soll beim entfernen der NFC-Karte passieren?
	 * 
	 * In der DB wird null gespeichert. Da sich keine Daten mehr am NFC Reader befinden.
	 * 
	 */
	
	@Override
	protected void cardIsRemoved() {
		LOG.info("Karte wurde entfernt!");
		try {
			cookieMacMapperOpt = terminalService.cookieMacMapperfindByMac(macReader.getMacAddess());
		} catch (SocketException e) {
			LOG.error("Fehler beim Lesen der MAC-Adresse!");
		}

		if (cookieMacMapperOpt.isPresent()){
			cookieMacMapper.setNfcId(null);
			cookieMacMapper = terminalService.saveNfcId(cookieMacMapper);
		}

	}



}
