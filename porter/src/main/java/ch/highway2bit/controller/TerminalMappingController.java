/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.highway2bit.entity.CookieMacMapper;
import ch.highway2bit.service.TerminalService;
import ch.highway2bit.utils.MacReader;

@Controller
@RequestMapping("/terminal/")
public class TerminalMappingController {

	@Autowired
	private TerminalService terminalService;

	// NAVIGATION
	private static final String MAP_ME = "terminal/mapme";

	// EVENTS
	private static final String EVENT_MAP_IT ="mapit";

	// ATTRIBUT
	private static final String ATTRIBUT_MAPPING_ID= "mappingId";

	private  MacReader macReader;

/**
 * Dieser Controller sendet die Cookie ID zum Frontend.
 * 
 * @param model
 * @return
 * @throws UnknownHostException
 * @throws SocketException
 */
	@GetMapping(EVENT_MAP_IT)
	public String index(Model model) throws UnknownHostException, SocketException{
		macReader = new MacReader();
		Optional<CookieMacMapper> opt = terminalService.cookieMacMapperfindByMac(macReader.getMacAddess());
		if (opt.isPresent()){
			model.addAttribute(ATTRIBUT_MAPPING_ID, opt.get().getId());
		}
		return MAP_ME;
	}

}
