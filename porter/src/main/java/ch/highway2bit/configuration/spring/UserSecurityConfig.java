/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.configuration.spring;



import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class UserSecurityConfig extends WebSecurityConfigurerAdapter {

	// Navigations TO
	private static final String NAV_ROOT ="/**";

	// Resources
	private static final String RES_RESOURCES ="/resources/**";
	private static final String RES_STATIC ="/static/**";
	private static final String RES_CSS ="/css/**";
	private static final String RES_JS ="/js/**";
	private static final String RES_IMAGES ="/images/**";
	private static final String RES_WEBJARS ="/webjars/**";

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers(RES_RESOURCES, RES_STATIC, RES_CSS, RES_JS, RES_IMAGES, RES_WEBJARS, NAV_ROOT);
    }

}
