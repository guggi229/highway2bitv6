/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.configuration.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.context.WebApplicationContext;


import ch.highway2bit.model.global.TerminalId;

/**
 * Hier werden zwei Sachen definiert:
 * 
 * 1. TerminalId wird als Application Scope verwendet
 * 
 * 2. Bean BCryptPasswordEncoder wird konfiguriert bzw. instanziert.
 * 
 * @author guggi229
 *
 */
@Configuration
public class TerminalSetup {


	@Bean
	@Scope(value = WebApplicationContext.SCOPE_APPLICATION)
	public TerminalId getTerminalIdModel() {
		return new TerminalId();
	}

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

}
