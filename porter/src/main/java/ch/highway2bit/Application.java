/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import ch.highway2bit.entity.CookieMacMapper;
import ch.highway2bit.service.TerminalService;
import ch.highway2bit.utils.MacReader;


@SpringBootApplication
@EnableScheduling
public class Application {

	@Autowired
	private TerminalService terminalService;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner init() {
		return (args) -> {
			MacReader macReader = new MacReader();

			Optional<CookieMacMapper> opt =  terminalService.cookieMacMapperfindByMac(macReader.getMacAddess());

			if(!opt.isPresent()){
			CookieMacMapper cookieMacMapper = new CookieMacMapper();
			cookieMacMapper.setMac(macReader.getMacAddess());
			terminalService.saveNfcId(cookieMacMapper);
			}
		};
	}
}

