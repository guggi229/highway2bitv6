package javaFx.Test;

import java.util.Arrays;
import java.util.List;

import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import testSetup.NFCcard;

public class ReadNFCOverJavaFX extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws CardException {
        primaryStage.setTitle("NFC Test");
        Button btn = new Button();
        btn.setText("Lies Karte");
        // get and print any card readers (terminals)
        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = factory.terminals().list();
        System.out.println("Terminals: " + terminals);
        
        // work with the first terminal
        CardTerminal term = terminals.get(0);
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
            	 try {
					if (term.isCardPresent()) {

					        // connect with the card. Throw an exception if a card isn't present
					        // the * means use any available protocol
					        Card card = term.connect("*");
					        System.out.println("card: " + card);

					        // Once we have the card, we can open a communication channel for sending commands and getting responses
					        CardChannel channel = card.getBasicChannel();

					        // Create the command for reading the UID - "FF CA 00 00 00"
					        // The smartcardio library wants a byte array. Bytes in java are signed numbers with a decimal value
					        // between -128 to 127. So if we want to use the hex codes from the documentation, we need to cast.
					        byte[] instruction = {(byte)0xFF, (byte)0xCA, (byte)0x00, (byte)0x00, (byte)0x00};
					        CommandAPDU getUID = new CommandAPDU(instruction);

					        // Send the command and print the response
					        // The response also comes as a byte array. If we want it to match standard format, we need to convert back to hex
					        // The first x bytes will be the UID, and the last 2 bytes will be SW1 and SW2.
					        // Success = [-112, 0] = [0x90, 0x00]
					        ResponseAPDU response = channel.transmit(getUID);
					        byte[] responseBytes = response.getBytes();
					        System.out.println("response: " + Arrays.toString(responseBytes));
					 }
				} catch (CardException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });

        StackPane root = new StackPane();
        root.getChildren().add(btn);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }
}
