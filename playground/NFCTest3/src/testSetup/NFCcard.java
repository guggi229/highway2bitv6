/***********************************************************
*      Copyright Stefan Guggisberg
*
*      Bachelor Arbeit BFH 2019-2020
*
************************************************************/

package testSetup;
import java.util.List;

import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

public class NFCcard {

    private TerminalFactory factory; 
    private List<CardTerminal> terminals;
    private CardTerminal terminal;
    private Card card ;
    public CardChannel cardChannel;
    
    byte buzzerOn = (byte)0xFF;
    byte buzzerOff = (byte)0x00;
    byte clazz = (byte)0xFF;
    byte ins = (byte)0x00;
    byte p1 = (byte)0x52;
    byte p2 = buzzerOff;
    byte le = (byte)0x00;

    

    public NFCcard() throws CardException {
        factory = TerminalFactory.getDefault();
        terminals = factory.terminals().list();
        terminal = terminals.get(0);
        card = terminal.connect("*");
        cardChannel = card.getBasicChannel();  
        
        byte[] apdu = new byte[]{clazz,ins,p1,p2,le};
        ResponseAPDU answer = cardChannel.transmit( new CommandAPDU(apdu));
        // cardChannel.transmit( new CommandAPDU(new byte[] { (byte)0xE0, (byte)0x00, (byte)0x00, (byte)0x21, (byte)0x01,(byte)0x77 }));
        byte successSW1 = (byte)0x90;
        byte successSW2 = (byte)0x00;
        if(answer.getSW1() == successSW1 && answer.getSW2() == successSW2){
            //done
        	System.out.println("ok");
        }else{
        	System.out.println("fail : " + answer);
            //failed
        }
    }



    public String getCardID() throws CardException{
        String cardID = "";
        ResponseAPDU answer=cardChannel.transmit( new CommandAPDU(new byte[] { (byte)0xFF, (byte)0xCA, (byte)0x00, (byte)0x00, (byte)0x00 }));
        byte r[] = answer.getData();
          for (int i=0; i<r.length; i++)
              cardID+=r[i];
        return cardID;
    }

}