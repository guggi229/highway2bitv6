package NFCLeseTest;


import javax.smartcardio.*;
import java.util.Arrays;
import java.util.List;


public class NFCTest1 {

    public static void main(String[] args) throws CardException {
       
        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = factory.terminals().list();
        System.out.println("Terminals: " + terminals);

       
        CardTerminal term = terminals.get(0);
        Card card = term.connect("*");
        System.out.println("card: " + card);

        CardChannel channel = card.getBasicChannel();

        byte[] instruction = {(byte)0xFF, (byte)0xCA, (byte)0x00, (byte)0x00, (byte)0x00};
        CommandAPDU getUID = new CommandAPDU(instruction);

            ResponseAPDU response = channel.transmit(getUID);
        byte[] responseBytes = response.getBytes();
        System.out.println("response: " + Arrays.toString(responseBytes));


    }

}
