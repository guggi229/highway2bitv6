package DurrationTest;

import java.time.LocalTime;
import java.time.Duration;

public class TestModel {

	
	private LocalTime localTime;
	
	private Duration duration;

	public LocalTime getLocalTime() {
		return localTime;
	}

	public void setLocalTime(LocalTime localTime) {
		this.localTime = localTime;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}
	
	
	
}
