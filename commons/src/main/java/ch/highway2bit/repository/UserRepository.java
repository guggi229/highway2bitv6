/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.highway2bit.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByEmailIgnoreCase(String email);

	User findById(long id);

	Optional<User> findUserByNfcCardId(String id);

	@Query("SELECT COUNT(u.id) FROM User u WHERE u.email= ?1 And u.password = ?2 AND  u.isDelete=false")
	Long checkPrincipals(String email, String encodedPassword);

	/**
	 * Sucht alle nicht gelöschte Elemente.
	 * @return
	 */
	@Query("SELECT u FROM User u WHERE u.isDelete=false")
	Set<User> findAllNotDeletedUser();



}