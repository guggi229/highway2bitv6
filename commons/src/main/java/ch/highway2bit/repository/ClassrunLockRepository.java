/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ch.highway2bit.entity.ClassrunLock;

public interface ClassrunLockRepository extends JpaRepository<ClassrunLock, Long> {

	public Optional<ClassrunLock> findByLockedBy(String mac);

}
