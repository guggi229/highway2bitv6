/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/


package ch.highway2bit.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.highway2bit.entity.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

	/**
	 * Sucht alle nicht gelöschte Elemente.
	 * @return
	 */
	@Query("SELECT r FROM Room r WHERE r.isDelete=false")
	Set<Room> findAllNotDeletedRoom();

}
