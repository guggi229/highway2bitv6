/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.highway2bit.entity.Kurs;

@Repository
public interface KursRepository extends JpaRepository<Kurs, Long> {

	/**
	 * Sucht alle nicht gelöschte Elemente.
	 * @return
	 */
	@Query("SELECT k FROM Kurs k WHERE k.isDelete=false")
	Set<Kurs> findAllNotDeletedKurs();

	Optional <Kurs> findByKursName(String name);

}