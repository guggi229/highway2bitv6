/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ch.highway2bit.entity.Terminal;

@Repository
public interface TerminalRepository extends JpaRepository<Terminal, String>  {

	Terminal getOne(String id);

	 long count();

	 Optional<Terminal> findById(String id);
}
