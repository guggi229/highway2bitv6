/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.highway2bit.entity.ParticipatedClass;
import ch.highway2bit.entity.User;
import ch.highway2bit.model.alias.ChargedState;


@Repository
public interface ParticipatedClassRepository extends JpaRepository<ParticipatedClass, Long> {

	Collection<ParticipatedClass> findByCharged(ChargedState charged);

	@Query("SELECT p.user FROM Kursrun k INNER JOIN Room r ON r.room_id = k.room.room_id INNER JOIN ParticipatedClass p ON k.id=p.kursrun.id where r.room_id=?1 AND k.date=?2 AND k.startTime between ?3 And ?4")
	Set<User> findBookedUser(Long roomId,LocalDate startDate, LocalTime localTime, LocalTime localTime2);



}
