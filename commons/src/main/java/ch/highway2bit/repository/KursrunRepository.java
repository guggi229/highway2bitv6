/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.highway2bit.entity.Kursrun;
import ch.highway2bit.entity.Room;

@Repository
public interface KursrunRepository extends JpaRepository<Kursrun, Long> {

	/**
	 * Sucht die nächste Kursdurchführung in einem bestimmten Raum.
	 * @param startDate
	 * @param localTime
	 * @param localTime2
	 * @param id
	 * @return
	 */
	@Query("select k from Kursrun k where k.date= ?1 AND k.startTime between ?2 And ?3 AND k.room.room_id= ?4")
	Optional<Kursrun> findNextClassRun(LocalDate startDate, LocalTime localTime, LocalTime localTime2, Long id);

	/**
	 * Sucht alle nicht gelöschte Elemente.
	 * @return
	 */
	@Query("SELECT k FROM Kursrun k WHERE k.isDelete=false")
	Set<Kursrun> findAllNotDeletedKurs();


	Set<Kursrun> findAllKursrunNotDeletedAndByDateAndRoom(LocalDate startDate, Room room);
}