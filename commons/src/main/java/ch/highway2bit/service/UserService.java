/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.security.Principal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.NfcCard;
import ch.highway2bit.entity.Role;
import ch.highway2bit.entity.User;
import ch.highway2bit.entity.subscription.SingleSubscription;
import ch.highway2bit.entity.subscription.Subscription;
import ch.highway2bit.entity.subscription.SubscriptionDefinition;
import ch.highway2bit.entity.subscription.SubscriptionTyp;
import ch.highway2bit.entity.subscription.TenSubscription;
import ch.highway2bit.entity.subscription.YearSubscription;
import ch.highway2bit.events.OnRegistrationCompleteEvent;
import ch.highway2bit.exception.ElementAlreadyExistException;
import ch.highway2bit.exception.EmailAddressAlreadyExistException;
import ch.highway2bit.exception.PasswordDoesNotMatchException;
import ch.highway2bit.exception.TokenExpireException;
import ch.highway2bit.exception.TokenNotFoundException;
import ch.highway2bit.exception.UserNotFoundException;
import ch.highway2bit.exception.WrongPasswordException;
import ch.highway2bit.model.global.Active;
import ch.highway2bit.model.user.register.VerificationToken;
import ch.highway2bit.repository.NfcCardRepository;
import ch.highway2bit.repository.RoleRepository;
import ch.highway2bit.repository.UserRepository;
import ch.highway2bit.repository.VerificationTokenRepository;

@Service
@Transactional
public class UserService {
	private static final Logger LOG = LoggerFactory.getLogger(UserService.class);


	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Autowired
	private VerificationTokenRepository tokenRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private SubscriptionDefinitionService subscriptionDefinitionService;

	@Autowired
	private NfcCardRepository nfcCardRepository;


	public User findUserByEmail(String email) {
		return userRepository.findByEmailIgnoreCase(email);
	}

	public Optional<User> findById(Long id){
		return userRepository.findById(id);
	}

	public List<User> findAll(){
		return userRepository.findAll();
	}

	public Set<User> finaAllNotDeletedUser(){
		return userRepository.findAllNotDeletedUser();
	}

	public User saveCustomer(User user) {
		Role userRole = roleRepository.findByRole("CUSTOMER");
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setActive(Active.ACTIVE);
		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		user= userRepository.save(user);
		return user;
	}

	public User newCustomer(User user, Long subscriptionDefinitionID) throws ElementAlreadyExistException, EmailAddressAlreadyExistException {
		Optional<NfcCard> opt;
		// Prüfe, ob die NCKarte bereits vergeben ist
		// ===========================================

		// 1. Wurde überhaupt eine ID eingeben?
		if(user.getNfcCard()!=null){
			opt = nfcCardRepository.findById(user.getNfcCard().getId());
			// 2. Ist sie bereits vorhanden?
			if (opt.isPresent()){
				throw new ElementAlreadyExistException();
			}
		}

		// Gibt es bereits diese Emailadresse?
		User dbUser = userRepository.findByEmailIgnoreCase(user.getEmail());
		if (dbUser!=null){
			throw new EmailAddressAlreadyExistException();

		}

		// Neues Abonnement erstellen
		Subscription subscirption = null;
		if (subscriptionDefinitionID!=null) {
			Optional <SubscriptionDefinition> optDef = subscriptionDefinitionService.findById(subscriptionDefinitionID);
			if (optDef.isPresent()) {

				if (optDef.get().getSubscriptionTyp().equals(SubscriptionTyp.ONE_YEAR)) {
					subscirption = new YearSubscription(LocalDate.now());
					subscirption.setSubscriptionDefinition(optDef.get());
				}
				else if(optDef.get().getSubscriptionTyp().equals(SubscriptionTyp.TEN_CARD)) {
					subscirption = new TenSubscription(0F);
					
					subscirption.setSubscriptionDefinition(optDef.get());
				}
				else if(optDef.get().getSubscriptionTyp().equals(SubscriptionTyp.SINGLE_ENTRY)) {
					subscirption = new SingleSubscription();
					subscirption.setSubscriptionDefinition(optDef.get());
				}
				user.setSubscirption(subscirption);

			}
		}

		// Diveres Properties
		Role userRole = roleRepository.findByRole("CUSTOMER");
		user.setActive(Active.IN_ACTIVE);
		user.setIsDelete(Boolean.FALSE);
		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		user= userRepository.save(user);

		// Auslösen des Registrierungs Email!
		eventPublisher.publishEvent(new OnRegistrationCompleteEvent	(user,null,null));
		return user;
	}

	/**
	 * Generiet einen neuen inaktiven Kunde und löst einen Event aus.
	 * @param user
	 * @return
	 * @throws ElementAlreadyExistException
	 * @throws EmailAddressAlreadyExistException
	 */
	public User newCustomer(User user) throws ElementAlreadyExistException, EmailAddressAlreadyExistException {
		return newCustomer(user, null);
	}

	public User save(User user) {
		return userRepository.save(user);
	}

	public User changePasswort(Principal principal, String oldPassword, String newPassword, String repeatedPassword) throws PasswordDoesNotMatchException, WrongPasswordException{
		User user = findUserByEmail(principal.getName());

		checkNewAndRepeatedPassword(newPassword ,repeatedPassword);

		// Prüfen, ob das alte Passwort NICHT mit dem Passwort in der DB stimmt.
		if (!bCryptPasswordEncoder.matches(oldPassword, user.getPassword())){
			throw new WrongPasswordException();
		}
		user.setPassword(bCryptPasswordEncoder.encode(newPassword));
		userRepository.save(user);
		return  user;
	}

	public User setNewPasswortByTokenId( String newPassword, String repeatedPassword, String tokenId) throws PasswordDoesNotMatchException, WrongPasswordException{

		VerificationToken verificationToken = tokenRepository.findByToken(tokenId);
		User user = verificationToken.getUser();
		checkNewAndRepeatedPassword(newPassword ,repeatedPassword);

		user.setPassword(bCryptPasswordEncoder.encode(newPassword));
		userRepository.save(user);
		return  user;
	}

	public void createVerificationToken(User user, String token) {
		VerificationToken verifyToken = new VerificationToken(user, token);
		tokenRepository.save(verifyToken);
	}

	public void checkToken(String tokenId) throws TokenNotFoundException, TokenExpireException {
		VerificationToken verificationToken = tokenRepository.findByToken(tokenId);
		if (verificationToken==null) {
			LOG.error("Token nicht gefunden");
			throw new TokenNotFoundException();
		}

		Calendar cal = Calendar.getInstance();

		if ((verificationToken.getExpiryDate().getTime()+ VerificationToken.EXPIRATION - cal.getTime().getTime()) <= 0) {
			LOG.error("Token abgelaufen");
			throw new TokenExpireException();
		}

	}
	/**
	 * Aktiviert einen zuvor registrierten User
	 * @param newPassword
	 * @param repeatedPassword
	 * @param tokenId
	 * @throws TokenNotFoundException
	 * @throws TokenExpireException
	 * @throws PasswordDoesNotMatchException
	 */
	public void activateUser (String newPassword, String repeatedPassword, String tokenId) throws TokenNotFoundException, TokenExpireException, PasswordDoesNotMatchException {
		checkToken(tokenId);
		checkNewAndRepeatedPassword(newPassword,repeatedPassword);
		VerificationToken verificationToken = tokenRepository.findByToken(tokenId);
		User user = verificationToken.getUser();
		user.setPassword(bCryptPasswordEncoder.encode(newPassword));
		user.setActive(Active.ACTIVE);
		tokenRepository.delete(verificationToken);
		save(user);
	}


	/**
	 * Prüfen ob die die Eingabe des neues Passworts gleich ist
	 * @param newPassword
	 * @param repeatedPassword
	 * @throws PasswordDoesNotMatchException
	 */
	private void checkNewAndRepeatedPassword(String newPassword, String repeatedPassword) throws PasswordDoesNotMatchException {
		if(!newPassword.equals(repeatedPassword)){
			throw new PasswordDoesNotMatchException();
		}
	}

	public void delete(long id) throws UserNotFoundException {
		Optional<User> opt = findById(id);
		if (opt.isPresent()){
			User user = opt.get();
			user.setIsDelete(Boolean.TRUE);
			save(user);
		}
		else {
			throw new UserNotFoundException();
		}

	}

	public User lockOrUnlockUser(long id) throws UserNotFoundException {
		User user;
		Optional<User> opt = findById(id);
		if (opt.isPresent()){
			user = opt.get();
			if (user.getActive().equals(Active.ACTIVE)){
				user.setActive(Active.IN_ACTIVE);
			} else {
				user.setActive(Active.ACTIVE);
			}
		}
		else {
			throw new UserNotFoundException();
		}

		return userRepository.save(user);

	}

}