/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.io.IOException;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.Kurs;
import ch.highway2bit.entity.Kursrun;
import ch.highway2bit.exception.TableLockedException;
import ch.highway2bit.exception.TimeOverlapException;
import ch.highway2bit.repository.KursrunRepository;

@Service
@Transactional
public class KursRunService {

	private static final Logger LOG = LoggerFactory.getLogger(KursRunService.class);
	@Autowired
	private KursrunRepository kursrunRepository;

	@Autowired
	private KursService kursService;

	@Autowired
	private ClassrunLockService classrunLockService;

	public List<Kursrun> findAll(){
		return kursrunRepository.findAll();
	}

	public Kursrun create(Kursrun kursrun) throws TimeOverlapException, TableLockedException {
		checkFreePlace(kursrun);
		kursrun.setIsDelete(false);
		return kursrunRepository.save(kursrun);
	}

	public Kursrun delete(Kursrun kurs) {
		kurs.setIsDelete(true);
		return kursrunRepository.saveAndFlush(kurs);
	}

	public Kursrun save(Kursrun kurs) {
		return kursrunRepository.save(kurs);
	}

	public Optional<Kursrun> findById(Long id){
		return kursrunRepository.findById(id);
	}

	public Set<Kursrun> findAllKursesNotDeleted(){
		return kursrunRepository.findAllNotDeletedKurs();
	}

	public Kursrun getDummyClass()  {
		Kurs dummy;
		dummy = kursService.getDummyKurs();
		Kursrun dummyrun = new Kursrun();
		dummyrun.setKurs(dummy);
		return dummyrun;
	}

	public void checkFreePlace(Kursrun newKurs) throws TimeOverlapException, TableLockedException{
		try (ClassrunLockService cls = classrunLockService ) {
			cls.lockClassRunTable();
			Set<Kursrun> kursruns = kursrunRepository.findAllKursrunNotDeletedAndByDateAndRoom(newKurs.getDate(), newKurs.getRoom() );
			LocalTime a = newKurs.getStartTime();
			LocalTime b = newKurs.getStartTime().plusMinutes(newKurs.getDuration().getTimeInMin());

			for (Kursrun classrunDB : kursruns) {
				LocalTime x = classrunDB.getStartTime();
				LocalTime y = classrunDB.getStartTime().plusMinutes(classrunDB.getDuration().getTimeInMin());

				if (b.isBefore(x)||( !b.isBefore(x) && !b.isBefore(y) && !a.isBefore(y))){
					LOG.info("Überlappt sich nicht");
				}
				else{
					LOG.error("Überlappung gefunden!");
					throw new TimeOverlapException();
				}
			}
		} catch (IOException e) {
			LOG.error("Überlappung gefunden!");
		}
	}

}
