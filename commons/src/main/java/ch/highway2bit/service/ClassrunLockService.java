/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;


import java.io.Closeable;
import java.io.IOException;
import java.net.SocketException;
import java.util.Optional;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.ClassrunLock;
import ch.highway2bit.exception.TableLockedException;
import ch.highway2bit.repository.ClassrunLockRepository;
import ch.highway2bit.utils.MacReader;

/**
 * Lockt die Tabelle aus Sicht Applikation.
 * 
 * Diese Lösung wurde benutzt, da diese H2 DB nicht komplatibel mit MySQL ist.
 * In einem späteren release sollte man Use Table benutzten.
 * @author guggi229
 *
 */

@Service
@Transactional(propagation=Propagation.REQUIRES_NEW)
public class ClassrunLockService implements Closeable {

	private static final Logger LOG = LoggerFactory.getLogger(ClassrunLockService.class);

	private MacReader macReader = new MacReader();

	@Autowired
	private ClassrunLockRepository classrunLockRepository;

	public void lockClassRunTable() throws TableLockedException {
		Long count = classrunLockRepository.count();
		if (count>0){
			LOG.info(" *** Lock gefunden ****");
			throw new TableLockedException();
		}
		ClassrunLock classrunLock = new ClassrunLock();
		try {
			classrunLock.setLockedBy(macReader.getMacAddess());
			LOG.info(" *** Tabelle gesperrt ****");
		} catch (SocketException e) {
			LOG.error("Mac Adresse kann nicht gelesen werden");
		}
		classrunLockRepository.save(classrunLock);
	}

	private void unlockClassRunTable() {
		try {
			/*
			 * Suche den Lock mit dieser MacAdresse
			 */
			Optional<ClassrunLock> opt = classrunLockRepository.findByLockedBy(macReader.getMacAddess());
			if (opt.isPresent()){
				classrunLockRepository.delete(opt.get());
			}
			else {
				/*
				 * Sollte nicht passieren können!
				 */
				LOG.error("Es ist nicht mein Lock! ");
			}
		} catch (SocketException e) {
			LOG.error("Mac Adresse kann nicht gelesen werden.");
		}
	}

	@Override
	public void close() throws IOException {
		unlockClassRunTable();
	}

}
