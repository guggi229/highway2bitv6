/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ch.highway2bit.entity.Kurs;
import ch.highway2bit.exception.ElementAlreadyExistException;
import ch.highway2bit.repository.KursRepository;


@Service
public class KursService  {
	private static final Logger LOG = LoggerFactory.getLogger(KursService.class);

	@Autowired
	private KursRepository kursRepository;

	public List<Kurs> findAll(){
		return kursRepository.findAll();
	}

	public Kurs create(Kurs kurs) throws ElementAlreadyExistException {
		checkForDuplicatedElement(kurs);
		kurs.setIsDelete(false);
		return kursRepository.save(kurs);
	}

	public Kurs delete(Kurs kurs) {
		kurs.setIsDelete(true);
		return kursRepository.saveAndFlush(kurs);
	}
/**
 * Regel 1: Hier wird geprüft, ob ein Element bereits so heisst (case sensitiv)
 *
 * Regel 2: Falls das Objekt bereits vorhanden ist, wird geprüft, ob es gelöscht ist. I diesem Fall würde ein neues Element erstellt.
 *
 * Regel 3: Beim Update wird in der Datenbank geprüft, ob dieser Name bereits vorhanden ist. Das eigene Element wird bei der Suche ausgeschlossen.
 * 			Damit beim Update ohne Änderung keine Exception geworfen wird.
 *
 * @param kurs
 * @throws ElementAlreadyExistException
 */
	private void checkForDuplicatedElement(Kurs kurs) throws ElementAlreadyExistException{

		Optional <Kurs> opt = kursRepository.findByKursName(kurs.getKursName());
		// Regel 1
		if (opt.isPresent()){
			// Regel 2: Prüfe, ob es gelöscht ist
			// Regel 3: Ist das Objekt zum Updaten sich selber?
			if (opt.get().getId()!=kurs.getId() && opt.get().getIsDelete().equals(Boolean.FALSE)){
				// Ist es nicht gelöscht, dann wäre es doppel vorhanden! --> Exception
				throw new ElementAlreadyExistException();
			}
		}
	}

	public Kurs save(Kurs kurs) throws ElementAlreadyExistException {
		checkForDuplicatedElement(kurs);
		return kursRepository.save(kurs);
	}

	public Optional<Kurs> findById(Long id){
		return kursRepository.findById(id);
	}

	public Set<Kurs> findAllKursesNotDeleted(){
		return kursRepository.findAllNotDeletedKurs();
	}

	public Kurs getDummyKurs() {
		Optional <Kurs> opt =  kursRepository.findByKursName("Kein Kurs");
		if (opt.isPresent()){
			LOG.info("Dummy Klasse geladen.");
			return opt.get();
		}
		else {
			LOG.error("**** Dummy Klasse nicht gefunden!. ****");
			return null;
		}
	}


}
