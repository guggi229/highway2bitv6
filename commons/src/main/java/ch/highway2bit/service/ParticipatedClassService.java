/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;



import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.ParticipatedClass;
import ch.highway2bit.entity.User;
import ch.highway2bit.repository.ParticipatedClassRepository;

@Service
@Transactional
public class ParticipatedClassService {

	@Autowired
	private ParticipatedClassRepository participatedClassRepository;

	public ParticipatedClass save(ParticipatedClass participatedClass) {
		return participatedClassRepository.save(participatedClass);
	}

	public Set<User> findUserByKursrun(Long id,LocalDate startDate, LocalTime localTime, LocalTime localTime2){
		return participatedClassRepository.findBookedUser(id,  startDate,  localTime,  localTime2);
	}



}
