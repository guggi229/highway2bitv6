/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.highway2bit.entity.Room;
import ch.highway2bit.repository.RoomRepository;

@Service
public class RoomService {

	@Autowired
	private RoomRepository repository;

	public Set<Room> findAllRoomsNotDeleted(){
		return repository.findAllNotDeletedRoom();
	}

	public Optional<Room> findById(Long id){
		return repository.findById(id);
	}

}
