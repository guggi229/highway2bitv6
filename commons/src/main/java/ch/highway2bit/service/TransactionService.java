/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.Transaction;
import ch.highway2bit.mode.transaction.TransactionState;
import ch.highway2bit.mode.transaction.TransactionTyp;
import ch.highway2bit.repository.TransactionRepository;

@Service
@Transactional
public class TransactionService {


	private DateTimeFormatter formatter;

	@Autowired
	private TransactionRepository transactionRepository;

	public Transaction save(Transaction transaction){
		return transactionRepository.save(transaction);
	}

	public Set<Transaction> findTransactionByCustomerId(Long customerId){
		return transactionRepository.findTransactionByUserId(customerId);
	}

	public TransactionService(){
		formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	}

	public Optional<Transaction> findById(String id){
		return transactionRepository.findById(id);
	}

	public Transaction createTransaction(TransactionTyp transactionTyp) {
		Transaction transaction = new Transaction();
		transaction.setRefNo(generateUniqueRefNo());
		transaction.setTransactionTyp(transactionTyp);
		transaction.setState(TransactionState.OFFEN);
		return save(transaction);
	}

	public Transaction createTransaction() {
		return createTransaction(null);
	}

	/**
	 * Generiert eine Unique Referenz Nummer, um die Transaction wieder zu finden.
	 * Bestehende aus dem Datum und einer Zufallszahl.
	 * @return Id
	 * @throws RefNoAlreadyExistException
	 */
	public String generateUniqueRefNo() {
		StringBuilder str  = new StringBuilder();
		String dateNow = LocalDateTime.now().format(formatter);
		str.append(dateNow);
		Random random = new Random();
		str.append(random.ints(1,(100000)).findFirst().getAsInt());
		String refNo = str.toString();
		return refNo;
	}

}
