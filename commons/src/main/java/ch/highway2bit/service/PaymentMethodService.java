/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.highway2bit.entity.PaymentMethod;
import ch.highway2bit.exception.PaymentMethodNotSupportedException;
import ch.highway2bit.repository.PaymentMethodRepository;

@Service
public class PaymentMethodService {

	@Autowired
	private PaymentMethodRepository paymentMethodRepository;

	public PaymentMethod getPaymentMethodeBySymbol(String symbol) throws PaymentMethodNotSupportedException {
		Optional<PaymentMethod> opt = paymentMethodRepository.findById(symbol);
		if(opt.isPresent()) {
			return opt.get();
		}
		throw new PaymentMethodNotSupportedException();
	}

	public PaymentMethod findBySymbol(String symbol){
		return paymentMethodRepository.findBySymbol(symbol);
	}
}
