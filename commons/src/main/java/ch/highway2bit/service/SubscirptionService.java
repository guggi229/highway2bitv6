/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.subscription.Subscription;
import ch.highway2bit.repository.SubscirptionRepository;

@Transactional
@Service
public class SubscirptionService {

	@Autowired
	private SubscirptionRepository subscirptionRepository;

	public Subscription save(Subscription subscirption){
		return subscirptionRepository.save(subscirption);
	}

	public List<Subscription> findAllSubscription(){
		return subscirptionRepository.findAll();
	}



}
