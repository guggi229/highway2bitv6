/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.subscription.SubscriptionDefinition;
import ch.highway2bit.repository.SubscriptionDefinitionRepository;

@Transactional
@Service
public class SubscriptionDefinitionService {

	@Autowired
	private SubscriptionDefinitionRepository subscriptionDefinitionRepository;

	public SubscriptionDefinition save(SubscriptionDefinition subscriptionDefinition){
		return subscriptionDefinitionRepository.save(subscriptionDefinition);
	}

	public List<SubscriptionDefinition> findAll() {
		return subscriptionDefinitionRepository.findAll();
		
	}
	
	public Optional<SubscriptionDefinition> findById(Long id) {
		return subscriptionDefinitionRepository.findById(id);
	}
	
	

}
