/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.net.SocketException;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.CookieMacMapper;
import ch.highway2bit.entity.Kursrun;
import ch.highway2bit.entity.NfcCard;
import ch.highway2bit.entity.ParticipatedClass;
import ch.highway2bit.entity.Terminal;
import ch.highway2bit.entity.User;
import ch.highway2bit.exception.NfcCardNotFoundException;
import ch.highway2bit.exception.NoAbonnementFound;
import ch.highway2bit.exception.ThereIsNonClassrunNowException;
import ch.highway2bit.exception.UserIsInactiveException;
import ch.highway2bit.exception.UserNotFoundException;
import ch.highway2bit.model.global.Active;
import ch.highway2bit.model.global.TerminalId;
import ch.highway2bit.repository.CookieMacMapperRepository;
import ch.highway2bit.repository.NfcCardRepository;
import ch.highway2bit.repository.ParticipatedClassRepository;
import ch.highway2bit.repository.TerminalRepository;
import ch.highway2bit.repository.UserRepository;
import ch.highway2bit.utils.MacReader;

@Service
@Transactional
public class TerminalService {
	private static final Logger LOG = LoggerFactory.getLogger(TerminalService.class);

	@Autowired
	private TerminalRepository terminalRepository;

	@Autowired
	private TerminalKursrunService kursrunService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ParticipatedClassRepository participatedClassRepository;

	@Autowired
	private NfcCardRepository nfcCardRepository;

	@Autowired
	private TerminalId terminalId;

	@Autowired
	private CookieMacMapperRepository cookieMacMapperRepository;

	private MacReader macreader = new MacReader();

	public Optional<Terminal> findById(String id) {
		return terminalRepository.findById(id);
	}

	public Long count() {
		return terminalRepository.count();
	}

	/**
	 * Prüfe was für eine NFC Karte kommt:
	 * Ist es eine NfcKarte, die mit einer Rolle belegt ist? --> Konfiguriere das Terminal auf die neue Rolle
	 * Wenn nicht, könnte es sich um eine Kundekarte handeln? Sende Info an bookCustomer
	 *
	 * @param nfcId
	 * @throws NoAbonnementFound
	 * @throws UserIsInactiveException
	 * @throws UserNotFoundException
	 * @throws ThereIsNonClassrunNowException
	 * @throws NfcCardNotFoundException
	 */
	public void checkJob(String nfcId) throws  UserNotFoundException, UserIsInactiveException, NoAbonnementFound, ThereIsNonClassrunNowException, NfcCardNotFoundException{
		Optional<NfcCard> nfcCardOpt = nfcCardRepository.findById(nfcId);
		if (nfcCardOpt.isPresent()){
			LOG.info("Karte in der DB gefunden");

			/********************************************
			 * Prüfe, ob die Karte zu einem Raum gehört
			 ********************************************/
			NfcCard card = nfcCardOpt.get();
			if (card.getRoom()==null){

				LOG.info("Info an BookCustomer. Könnte es eine Kunde Karte sein?");
				/********************************************
				 * Prüfe, ob die Karte zu einem Kunden gehört
				 ********************************************/
				bookCustomer(nfcId);
			}

			else
			{
				LOG.info("Rolle Karte gefunden!");
				setNewRoom(nfcCardOpt.get());
			}
		}
		else {
			LOG.info("Karten ID in der DB nicht vorhanden");
			throw new NfcCardNotFoundException();


		}

	}

	public Optional<CookieMacMapper> cookieMacMapperfindByMac(String mac){
		return cookieMacMapperRepository.findByMac(mac);
	}

	public Optional<CookieMacMapper> cookieMacMapperfindById(Long id){
		return cookieMacMapperRepository.findById(id);
	}

	public CookieMacMapper saveNfcId(CookieMacMapper cookieMacMapper){
		return cookieMacMapperRepository.save(cookieMacMapper);
	}

	/**
	 * Diese Methode prüft die Business Logik beim Einchecken eines Kunden in einen Groupfitness Kurs.
	 * Geprüft wird:
	 *
	 * 			1. Gibt es überhaupt einen Groupfitness Kurs?
	 * 			2. Ist die NFC ID, also die Karte, mit einem Kunden gemappt?
	 * 			3. Ist der Kunde aktiv?
	 * 			4. Hat er ein Zahlungsmittel registriert oder hat er ein Jahreabo?
	 *
	 * Sind alle Bedinungen erfüllt, wird der Kunde als Teilnehmer erfasst.
	 *
	 * @param nfcId
	 * @throws ThereIsNonClassrunNowException
	 * @throws UserNotFoundException
	 * @throws UserIsInactiveException
	 * @throws NoAbonnementFound
	 */
	private void bookCustomer(String nfcId) throws ThereIsNonClassrunNowException, UserNotFoundException, UserIsInactiveException, NoAbonnementFound{
		Kursrun kursrun;
		User user;
		// Suche nächsten Kurs
		Optional<Kursrun> kursrunOpt = kursrunService.findNextKursrun();

		// Suche User
		Optional<User> userOpt = userRepository.findUserByNfcCardId(nfcId);

		// Gibt es überhaupt einen Groupfitness Kurs?
		if (kursrunOpt.isPresent() && !kursrunOpt.get().getId().equals(0L)){
			kursrun = kursrunOpt.get();
			LOG.info("Kurs vorhanden");

		} else{
			LOG.info("Im Moment findet kein Kurs statt.");
			throw new ThereIsNonClassrunNowException();
		}

		// Ist die NFC ID, also die Karte, mit einem Kunden gemappt?
		if (userOpt.isPresent()){
			user=userOpt.get();
			LOG.info("User vorhanden");
		} else {
			LOG.info("Kein User mit NFC ID {} gefunden", nfcId);
			throw new UserNotFoundException();
		}

		// Ist der Kunde aktiv?
		if (user.getActive().equals(Active.IN_ACTIVE)){
			throw new UserIsInactiveException();
		}

		LOG.info("*** Daten am NFC Reader plausibel *** ");

		//Dann scheint alles OK zu sein
		ParticipatedClass partClass = new  ParticipatedClass();
		partClass.setKursrun(kursrun);
		partClass.setUser(user);

		participatedClassRepository.save(partClass);

	}

	private void setNewRoom(NfcCard nfcCard) {

		Terminal terminal=null;
		// Fall 1:
		// Terminal wurde noch nicht zugewiesen
		if(terminalId.getMac()==null){
			LOG.info("Neues Terminal!");
			try {
				// Merke dir deine MAC-Adresse:
				terminalId.setMac(macreader.getMacAddess());

			} catch (SocketException e) {
				LOG.error("MAC Adresse konnte nicht gelesen werden!");
				System.exit(0);
			}
			terminal = new Terminal();
			terminal.setMac(terminalId.getMac());
		}
		terminalId.setRoom(nfcCard.getRoom());
		// Fall 2:
		// Info ist in der Datenbank vorhanden
		Optional<Terminal> terminalOpt = terminalRepository.findById(terminalId.getMac());
		if(terminalOpt.isPresent()){
			terminal = terminalOpt.get();
		}
		// Daten speichern
		terminal.setRoom(nfcCard.getRoom());
		terminal.setMac(terminalId.getMac());
		
		terminalRepository.save(terminal);
		LOG.info(" *** Das Terminal wurde für Raum {} konfiguriert:" , nfcCard.getRoom().getName());
	}

}
