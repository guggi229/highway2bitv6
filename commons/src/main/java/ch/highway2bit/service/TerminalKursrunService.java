/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.highway2bit.entity.Kursrun;
import ch.highway2bit.model.global.TerminalId;
import ch.highway2bit.repository.KursrunRepository;

@Service
public class TerminalKursrunService {

	private static final Logger LOG = LoggerFactory.getLogger(TerminalKursrunService.class);
	private long slidingWindow = 10;

	@Autowired
	private KursrunRepository kursrunRepository;

	@Autowired
	private TerminalId terminalId;


	public Optional<Kursrun> findNextKursrun(){
		// Sollte das Terminal nicht konfiguriert sein, dass beende die Applikation
		if(terminalId.getRoom()==null){
			LOG.error("Terminal ist unbbekannt!");
			System.exit(0);
		}
		LOG.info("Suche nächsten Kurs für Raum {}", terminalId.getRoom().getName());
		LOG.info("Zeit:" + LocalTime.now());
		LOG.info("Datum:" + LocalDate.now());
		Optional<Kursrun> opt = kursrunRepository.findNextClassRun(LocalDate.now(), LocalTime.now().minusMinutes(slidingWindow), LocalTime.now().plusMinutes(slidingWindow), terminalId.getRoom().getRoom_id());
		return opt;
	}

}
