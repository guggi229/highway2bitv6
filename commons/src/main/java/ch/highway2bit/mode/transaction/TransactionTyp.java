/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.mode.transaction;


/**
 * Es gibt verschiedene Transaktionarten. Diese werden hier definiert.
 * 
 * Der Text ist selbsterklärend.
 * 
 * @author guggi229
 *
 */
public enum TransactionTyp {
	CREATE_ALIAS(0), PAY_WITH_ALIAS(1), PAY_WITHOUT_ALIAS(2), DELETE_ALIAS(3), YEAR_SUBSCRIPTION(4);

	private int code;

	TransactionTyp(int code){
		this.code=code;
	}

	public int getCode() {
		return code;
	}
}
