/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.mode.transaction;


/**
 * Jede Transaktion kann verschiedene Stati haben. Sie sind selbsterklärend.
 * @author guggi229
 *
 */
public enum TransactionState {
	OFFEN(0), GEBUCHT(1), ABGELEHNT(2), ABGEBROCHEN(3), OK(4), ERROR(5);

	private int code;

	TransactionState(int code){
		this.code=code;
	}

	public int getCode() {
		return code;
	}
}
