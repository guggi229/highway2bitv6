/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacReader {
	private static final Logger LOG = LoggerFactory.getLogger(MacReader.class);

	/**
	 * Liest die Mac Adresse der Netzwerkkarte. Auf dem Raspian gibt es  ein Problem mit Java 8. Daher wurde sie statisch gesetzt.
	 * Bei Java 9 sollte es dann wieder gehen.
	 *
	 * @return
	 * @throws SocketException
	 */
	public String getMacAddess() throws SocketException{
		StringBuilder sb = new StringBuilder();
		InetAddress ip;
		try {
			ip = InetAddress.getLocalHost();
			LOG.info("IP: {}" , ip.getHostAddress());
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			byte[] mac = network.getHardwareAddress();

			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			LOG.info("MAC-ADDRESS: {} ",sb.toString());

		} catch (Exception e) {
			LOG.error("Unbekannter Fehler ist aufgetreten: {}",e);
		}
	//	return sb.toString();
		return "02-00-4C-4F-4F-50";
	}

}
