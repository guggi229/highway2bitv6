/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.utils;
/**
 * Öffnet nur so viel wie nötig! Wir sichern die Strings, damit nicht unerlaubte Zeichen drin sind.
 * @author guggi229
 *
 */
public final class RegexConstants {

	// Email:
	public final static String REGEX_EMAIL="^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
	public final static String TEXT_EMAIL_ERROR = "Email Adresse nicht gültig";

	// Name:
	public final static String ERLAUBTE_ZEICHEN_NAME="a-zA-Z0-9\\söäüÖÄÜ";
	public final static String REGEX_NAME="^["+ ERLAUBTE_ZEICHEN_NAME +"]{1,50}$";
	public final static String TEXT_NAME_ERROR = "Eingabe nicht gültig! Erlaubt sind: " + ERLAUBTE_ZEICHEN_NAME + "Länge 1 bis 50";

	// Passwort:
	public static final String REGEX_PASSWORT_ENCODED = "^\\$2[ayb]\\$.{56}$";
	public final static String ERLAUBTE_ZEICHEN_PASSWORT_PLAIN = "a-zA-Z0-9öäüÖÄÜ!#$%&";
	public static final String REGEX_PASSWORT_PLAIN ="^["+ ERLAUBTE_ZEICHEN_PASSWORT_PLAIN + "]{1,50}$";
	public final static String TEXT_PASSWORT_ERROR = "Eingabe nicht gültig";

	// Alias Regex
	// Liste der möglichen Alias String (AN20 und UUID Format). Dabei für Paypal inklusiv Sonderzeichen '-')
	// https://docs.datatrans.ch/docs/payment-process-alias
	public static final String REGEX_ALIAS_AN20="^[0-9a-zA-Z]{1,20}$|^[0-9a-zA-Z]{1}[-]{1}[0-9a-zA-Z]{2,18}$";
	public static final String REGEX_ALIAS_UUID="^[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}$";


	// Error Meldungen generell
	public final static String TEXT_FORMAT_NAME_ERROR = "Muss ausgefüllt werden. Zeichen: Buchstaben und Zahlen!";

	// Abo Info
	public final static String REGEX_NAME_INFO_ABO="^[0-9a-zA-Z\\söäüÖÄÜ]{1,40}$";


	// Constans
	public static final int ONE = 1;


	private RegexConstants() {

	}

}
