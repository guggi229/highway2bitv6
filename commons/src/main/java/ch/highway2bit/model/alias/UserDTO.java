/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.alias;

import java.io.Serializable;

import ch.highway2bit.entity.Alias;

public class UserDTO implements Serializable {

	private static final long serialVersionUID = 8123807446694014294L;

	private String email;
	private String name;
	private Alias alias;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Alias getAlias() {
		return alias;
	}
	public void setAlias(Alias alias) {
		this.alias = alias;
	}
	
	
}
