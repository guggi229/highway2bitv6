/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.alias;


public enum ChargedState {
	OPEN(0),ERROR(1), OK(2);

	private int code;

	ChargedState(int code){
		this.code=code;
	}

	public int getCode() {
		return code;
	}
}
