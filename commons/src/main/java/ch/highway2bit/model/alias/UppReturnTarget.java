/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.alias;

/**
 * Hilsf Enum für das Erstellen des Requets.
 * 
 * @author guggi229
 *
 */
public enum UppReturnTarget {
	_top, lightbox
}
