/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.alias;

import java.io.Serializable;

/**
 * Die Antwort auf den AliasRequest wird hier im AliasResponse gehalten.
 * 
 * @author guggi229
 *
 */
public class AliasRegisterResponeModel implements Serializable{
	private static final long serialVersionUID = 8456973023699609396L;

	private Long authorizationCode;

	private String maskedCC;

	private String sign;

	private String aliasCC;

	private Integer responseCode;

	private String expy;

	private String expm;

	private Integer merchantId;

	private Integer acqAuthorizationCode;

	private String reqtype;

	private String uppWebResponseMethod;

	private String currency;

	private String refno;

	private String pmethod;

	private String status;

	public Long getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(Long authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getMaskedCC() {
		return maskedCC;
	}

	public void setMaskedCC(String maskedCC) {
		this.maskedCC = maskedCC;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getAliasCC() {
		return aliasCC;
	}

	public void setAliasCC(String aliasCC) {
		this.aliasCC = aliasCC;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public String getExpy() {
		return expy;
	}

	public void setExpy(String expy) {
		this.expy = expy;
	}

	public String getExpm() {
		return expm;
	}

	public void setExpm(String expm) {
		this.expm = expm;
	}

	public Integer getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}

	public Integer getAcqAuthorizationCode() {
		return acqAuthorizationCode;
	}

	public void setAcqAuthorizationCode(Integer acqAuthorizationCode) {
		this.acqAuthorizationCode = acqAuthorizationCode;
	}

	public String getReqtype() {
		return reqtype;
	}

	public void setReqtype(String reqtype) {
		this.reqtype = reqtype;
	}

	public String getUppWebResponseMethod() {
		return uppWebResponseMethod;
	}

	public void setUppWebResponseMethod(String uppWebResponseMethod) {
		this.uppWebResponseMethod = uppWebResponseMethod;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getRefno() {
		return refno;
	}

	public void setRefno(String refno) {
		this.refno = refno;
	}

	public String getPmethod() {
		return pmethod;
	}

	public void setPmethod(String pmethod) {
		this.pmethod = pmethod;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


}
