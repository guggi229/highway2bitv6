/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.user.register;

import java.io.Serializable;

import javax.validation.constraints.Pattern;

import ch.highway2bit.utils.RegexConstants;

public class ChangePasswordTransactionModel implements Serializable{

	private static final long serialVersionUID = 8438537363494835075L;

	@Pattern(regexp = RegexConstants.REGEX_PASSWORT_PLAIN, message = RegexConstants.TEXT_PASSWORT_ERROR)
	private String password;

	@Pattern(regexp = RegexConstants.REGEX_PASSWORT_PLAIN, message = RegexConstants.TEXT_PASSWORT_ERROR)
	private String newPassword;

	@Pattern(regexp = RegexConstants.REGEX_PASSWORT_PLAIN, message = RegexConstants.TEXT_PASSWORT_ERROR)
	private String repeatedNewPassword;

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getRepeatedNewPassword() {
		return repeatedNewPassword;
	}
	public void setRepeatedNewPassword(String repeatedNewPassword) {
		this.repeatedNewPassword = repeatedNewPassword;
	}


}
