/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model;


/**
 * Ist das Transferobjekt für den WebSocket Request.
 * 
 * @author guggi229
 *
 */
public class MessageContainer {

    private String content;

    public MessageContainer() {
    }

    public MessageContainer(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
