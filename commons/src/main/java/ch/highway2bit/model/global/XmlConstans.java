/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.global;

/**
 * Hält Konstante um das XML zu bauen.
 * 
 * @author guggi229
 *
 */
public final class XmlConstans {

	// RequestTyp

	public static final String ACCEPT = "Accept";
	public static final String ACCEPT_VALUE = "application/xml";

	public static final String CONTENT_TYPE="Content-Type";
	public static final String CONTENT_TYPE_VALUE="application/xml";

	public static final String AUTHORIZATION = "Authorization";

	public static final String ENCODING = "UTF-8";

	public static final int STREAM_SIZE = 2048;

	public static final int ZERO = 0;

	private XmlConstans(){

	}

}
