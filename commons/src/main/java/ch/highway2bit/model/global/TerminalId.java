/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.global;


import java.io.Serializable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import ch.highway2bit.entity.Room;



/**
 * Hält die Information über die Terminal ID und Raum.
 * 
 * @author guggi229
 *
 */
public class TerminalId implements Serializable  {

	private static final long serialVersionUID = -4859161067651424146L;

	private String mac;

	private Room room;

	@Enumerated(EnumType.ORDINAL)
	private TerminalRole role;

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public TerminalRole getRole() {
		return role;
	}

	public void setRole(TerminalRole role) {
		this.role = role;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

}
