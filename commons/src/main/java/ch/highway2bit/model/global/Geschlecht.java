/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/package ch.highway2bit.model.global;

public enum Geschlecht {
	Mann, Frau, offen
}
