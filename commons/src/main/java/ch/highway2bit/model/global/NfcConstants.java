/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.global;


public final class NfcConstants {

	// ISO 7816-4 APDU Format
	public static final byte CLASS = (byte) 0xFF;
	public static final byte INS_BUZZER = (byte) 0x00; // Entspricht dem Befehl ()Hier für Bzzer
	public static final byte INS_GET_CARD_ID = (byte) 0xCA; // Entspricht dem Befehl ()Hier für Bzzer
	public static final byte LE=(byte) 0x00; // Expected length of the Response Data

	// Params
	public static final byte PARAM_P1= (byte)0x52;
	public static final byte PARAM_BUZZER_OFF_P2=(byte) 0x00;
	public static final byte PARAM_BUZZER_ON_P2=(byte) 0xFF;
	public static final byte PARAM_GET_CARD_ID_P1=(byte) 0x00;
	public static final byte PARAM_GET_CARD_ID_P2=(byte) 0x0;

	//Antwort ok Entspricht 90 00h Entspricht -112 0
	public static final byte RESPONSE_SW1_OK =(byte)0x90;
	public static final byte RESPONSE_SW2_OK=(byte)0x00;

	//Antwort ok Entspricht 90 00h Entspricht -112 0
	public static final byte RESPONSE_SW1_NOK =(byte)0x63;
	public static final byte RESPONSE_SW2_NOK=(byte)0x00;

	private NfcConstants(){

	}

	/**
	 * Baut das APDU so zusammen, dass der Buzzer ausgeschaltet wird
	 */
	public static byte[] getParamBuzzerOff(){
		return new byte[]{CLASS,INS_BUZZER,PARAM_P1,PARAM_BUZZER_OFF_P2,LE};
	}

	/**
	 * Sendet dem NFC reader den Befehl, die NFC-Karte auszulesen. Mehr Infos unter API SPEZ Seite 34 in ACR122U – Application Programming Interface
	 * @return
	 */
	public static byte[] getParamReadID(){
		return new byte[]{CLASS,INS_GET_CARD_ID,PARAM_GET_CARD_ID_P1,PARAM_GET_CARD_ID_P2,LE};
	}

}
