/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.global;
/**
 * Wird verwendet, um einen Uer aktiv oder inaktiv zu setzten.
 * 
 * @author guggi229
 *
 */
public enum Active {
	IN_ACTIVE(0), ACTIVE(1);

	private int code;

	Active(int code){
		this.code=code;
	}

	public int getCode() {
		return code;
	}
}
