/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.pay;

/**
 * Repräsentiert die Datenstruktur die von datatrans auf https://api-reference.datatrans.ch/ zu finden ist.
 *
 * @author guggisberste
 *
 */
public final class PayResponseConstans {

	public final static String REGEX_AUTHORIZATION_CODE="^[0-9]{1,9}$";
	public final static String REGEX_RESPONSE_CODE ="^[0-9]{1,10}$";
	public final static String REGEX_RESPONSE_MESSAGE="^[a-zA-Z0-9öäüÖÄÜ]{0,200}$";
	public final static String REGEX_UPP_TRANSACTION_ID="^[0-9]{1,18}$";
	public final static String REGEX_ACQ_AUTHORIZATION_CODE="^[a-zA-Z0-9]{0,50}$";
	public final static String REGEX_ALIAS_CC="^[a-zA-Z0-9]{0,50}$";
	
	public final static String RESPONSE_CODE_FROM_DATATRANS ="01";


	private PayResponseConstans(){

	}

}
