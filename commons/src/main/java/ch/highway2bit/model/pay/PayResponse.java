/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.pay;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

import ch.highway2bit.model.pay.PayResponseConstans;
import ch.highway2bit.utils.RegexConstants;

/**
 * Speichert die Antwort von Datatrans
 * @author guggisberste
 *
 */
@Entity
public class PayResponse implements Serializable{

	private static final long serialVersionUID = 1922032974723734807L;

	@Id
	@Pattern(regexp = PayResponseConstans.REGEX_AUTHORIZATION_CODE)
	private String  authorizationCode;

	@Pattern(regexp = PayResponseConstans.REGEX_RESPONSE_CODE)
	private String responseCode;

	@Pattern(regexp = PayResponseConstans.REGEX_RESPONSE_MESSAGE)
	private String responseMessage;

	@Pattern(regexp = PayResponseConstans.REGEX_UPP_TRANSACTION_ID)
	private String uppTransactionId;

	@Pattern(regexp = PayResponseConstans.REGEX_ACQ_AUTHORIZATION_CODE)
	private String acqAuthorizationCode;

	@Pattern(regexp = RegexConstants.REGEX_ALIAS_AN20 + "|" + RegexConstants.REGEX_ALIAS_UUID)
	private String aliasCC;

	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getUppTransactionId() {
		return uppTransactionId;
	}
	public void setUppTransactionId(String uppTransactionId) {
		this.uppTransactionId = uppTransactionId;
	}
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getAcqAuthorizationCode() {
		return acqAuthorizationCode;
	}
	public void setAcqAuthorizationCode(String acqAuthorizationCode) {
		this.acqAuthorizationCode = acqAuthorizationCode;
	}
	public String getAliasCC() {
		return aliasCC;
	}
	public void setAliasCC(String aliasCC) {
		this.aliasCC = aliasCC;
	}


}
