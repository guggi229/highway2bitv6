/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model.pay;

import ch.highway2bit.entity.User;
import ch.highway2bit.exception.PaymentException;

public interface PayServiceInterface {

	public void pay(Long betrag, User user) throws PaymentException;
}
