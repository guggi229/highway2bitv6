/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model;

/**
 * Es gibt drei Arten von Kusre. Solche die 30, 60 oder 90 Minuten dauern. Dies wird hier definiert.
 * 
 * @author guggi229
 *
 */
public enum ClassRunDuration {

	DREISSIG_MINUTEN(30), SECHZIG_MINUTEN(60), NEUNZIG_MINUTEN(90);

	private int timeInMin;

	ClassRunDuration(int timeInMin){
		this.timeInMin=timeInMin;
	}

	public int getTimeInMin() {
		return timeInMin;
	}
}
