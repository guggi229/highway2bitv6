/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
 *************************************************************************************/

package ch.highway2bit.terminal;
import javax.smartcardio.*;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.highway2bit.model.global.NfcConstants;
import ch.highway2bit.service.TerminalService;

/**
 * Ist das Grundgerüst für das Lesen des NFC-Readers. 
 * @author guggi229
 *
 */
@SuppressWarnings("restriction")
public abstract class TerminalTemplate {


	protected static final Logger LOG = LoggerFactory.getLogger(TerminalTemplate.class);
	private static final int TERMINAL_NUMMER = 0;

	//private NfcObservable nfcObservable = new NfcObservable();

	private TerminalFactory factory;
	private List<CardTerminal> terminals;
	private CardTerminal terminal;
	private Card card ;
	private CardChannel cardChannel;
	private boolean cardChecked=false;
	protected TerminalService terminalService;


	public TerminalTemplate(){
		try {
			// Terminal vorbereiten. Es wird nur das erste Terminal genommen.
			factory = TerminalFactory.getDefault();
			terminals = factory.terminals().list();
			terminal = terminals.get(TERMINAL_NUMMER);
			LOG.info("Terminal verbunden");
		} catch (CardException e) {
			LOG.error(" **** Terminal nicht gefunden ****");
		}
	}

	/**
	 * Was soll mit den Daten aus dem NFC-Readers geschehen?
	 * Diese Methode verarbeitet die Daten daraus!
	 */
	protected abstract void terminalJob(byte[] answer);

	/**
	 * Was soll geschehen, wenn jemand die Karte entfernt?
	 */
	protected abstract void cardIsRemoved();

	/**
	 * Für alle NFC-Reader gilt das selbe Setup. Daher ist dieser Fix verdrahtet.
	 */
	private final void preReceiver() {
		try {
			if(terminal!=null && !cardChecked && terminal.isCardPresent()){
				// Karte nur einmal lesen
				cardChecked=true;

				// reader lesen
				card = terminal.connect("*");
				cardChannel = card.getBasicChannel();

				// Buzzer auf off
				cardChannel.transmit( new CommandAPDU(NfcConstants.getParamBuzzerOff()));
				ResponseAPDU answer=cardChannel.transmit( new CommandAPDU(NfcConstants.getParamReadID()));
				LOG.info("Karte {} gefunden.", bytesToHex(answer.getData()) );

				// Prüfe auf die Aufgabe
				terminalJob(answer.getData());

			}
			else if(terminal!=null &&!terminal.isCardPresent()){
				if (cardChecked){
					cardIsRemoved();
				}
				cardChecked=false;
			}
		}
		catch ( CardException  e) {
			cardIsRemoved();
			cardChecked=false;
			LOG.info("Karte wurde entfernt"); // Kein unerwartetes Ereignis.
		}
	}


	/**
	 * Sollte nur durch den SheduleTask aufgerufen werden!
	 * 
	 * @param terminalService
	 */
	public void readReceiver(TerminalService terminalService ) {
		this.terminalService=terminalService;
		preReceiver();
	}


	/**
	 * Wandelt Bye nach String um
	 * @param hashInBytes
	 * @return
	 */
	protected static String bytesToHex(byte[] hashInBytes) {
		StringBuilder sb = new StringBuilder();
		for (byte b : hashInBytes) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}

}
