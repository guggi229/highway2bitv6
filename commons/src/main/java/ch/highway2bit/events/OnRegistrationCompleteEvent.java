/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.events;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;

import ch.highway2bit.entity.User;

/**
 * Generiert einen Event sobald ein neuer User erstellt wurde.
 * @author guggi229
 *
 */
public class OnRegistrationCompleteEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1231146488247813720L;
	private static final Logger LOG = LoggerFactory.getLogger(OnRegistrationCompleteEvent.class);

	private String appUrl;
	private Locale locale;
	private User user;


	public OnRegistrationCompleteEvent(User user, Locale locale, String appUrl) {
		super(user);
		LOG.info("Neuer User Event");
		this.user = user;
		this.locale = locale;
		this.appUrl = appUrl;

	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
