/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;

import ch.highway2bit.model.global.Currency;
import ch.highway2bit.utils.RegexConstants;

@Embeddable
public class Alias implements Serializable {
	private static final long serialVersionUID = 2602170320882312408L;

	@ManyToOne
	private PaymentMethod pmethode;

	private Currency currency;

	@Pattern(regexp = RegexConstants.REGEX_ALIAS_AN20 + "|" + RegexConstants.REGEX_ALIAS_UUID)
	private String aliasCc;

	private String expiryMonth;

	private String expiryYear;

	private String maskedCreditCardNumber;


	public PaymentMethod getPmethode() {
		return pmethode;
	}

	public void setPmethode(PaymentMethod pmethode) {
		this.pmethode = pmethode;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getAliasCc() {
		return aliasCc;
	}

	public void setAliasCc(String aliasCc) {
		this.aliasCc = aliasCc;
	}

	public String getExpiryMonth() {
		return expiryMonth;
	}

	public String getExpiryYear() {
		return expiryYear;
	}

	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}

	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}

	public String getMaskedCreditCardNumber() {
		return maskedCreditCardNumber;
	}

	public void setMaskedCreditCardNumber(String maskedCreditCardNumber) {
		this.maskedCreditCardNumber = maskedCreditCardNumber;
	}




}
