/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CookieMacMapper implements Serializable {

	private static final long serialVersionUID = 4098738202817618586L;

	@Id
	@Column(name = "COOKIE_ID")
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private Long id;


	@Column(name = "MAC_ID",unique = true)
	private String mac;

	@Column(name = "NFC_ID")
	private String NfcId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getNfcId() {
		return NfcId;
	}

	public void setNfcId(String nfcId) {
		NfcId = nfcId;
	}



}
