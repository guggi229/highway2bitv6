/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/


package ch.highway2bit.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class Room implements Serializable {

	private static final long serialVersionUID = -3943709856931193519L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="room_id")
	private Long room_id;

	@Column(name="name", unique=true)
	private String name;

	@Column(name="deleted")
	private Boolean isDelete;


	@OneToMany(  cascade = {CascadeType.PERSIST,CascadeType.MERGE},fetch = FetchType.EAGER, mappedBy="room")
	@Column(name="nfcCards")
	private Set <NfcCard> nfcCards = new HashSet<>();

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

	public Long getRoom_id() {
		return room_id;
	}

	public void setRoom_id(Long room_id) {
		this.room_id = room_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Set<NfcCard> getNfcCards() {
		return nfcCards;
	}

	public void addNfcCards(NfcCard nfcCard) {
		this.nfcCards.add(nfcCard);
	}

	public void setNfcCards(Set<NfcCard> nfcCards) {
		this.nfcCards = nfcCards;
	}



}
