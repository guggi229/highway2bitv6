/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import ch.highway2bit.model.global.Geschlecht;
import ch.highway2bit.utils.RegexConstants;
import ch.highway2bit.entity.subscription.Subscription;
import ch.highway2bit.model.global.Active;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "user")
public class User implements Serializable{

	private static final long serialVersionUID = -5668894499824777226L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long id;

	@Pattern(regexp = RegexConstants.REGEX_EMAIL, message = RegexConstants.TEXT_EMAIL_ERROR)
	@Column(name = "email",unique = true)
	private String email;

	@Pattern(regexp = RegexConstants.REGEX_PASSWORT_ENCODED, message = RegexConstants.TEXT_PASSWORT_ERROR)
    @Column(name = "password")
    private String password;

    @Pattern(regexp = RegexConstants.REGEX_NAME, message = RegexConstants.TEXT_NAME_ERROR)
    @Column(name = "name")
    private String name;

    @Pattern(regexp = RegexConstants.REGEX_NAME, message = RegexConstants.TEXT_NAME_ERROR)
    @Column(name = "vorname")
    private String vorname;

    @Column(name="geschlecht")
    private Geschlecht geschlecht;

    @Column(name = "active")
    private Active active;

	@Column(name="deleted")
	private Boolean isDelete;

    @OneToOne(cascade = CascadeType.ALL)
    private NfcCard nfcCard;


    @ManyToMany(  cascade = {CascadeType.MERGE},fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<Role>();

    @OneToOne(cascade = CascadeType.ALL )
    private Subscription subscirption;

    @Embedded
    private Alias alias;

    @OneToMany(mappedBy="user",cascade = CascadeType.ALL)
    private Set<Transaction> transactions;

    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private Set<ParticipatedClass> participatedClassruns = new HashSet<>();

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Active getActive() {
		return active;
	}

	public void setActive(Active active) {
		this.active = active;
	}

	public NfcCard getNfcCard() {
		return nfcCard;
	}

	public void setNfcCard(NfcCard nfcCard) {
		this.nfcCard = nfcCard;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void addRole(Role role) {
		this.roles.add(role);
	}

	public Alias getAlias() {
		return alias;
	}

	public void setAlias(Alias alias) {
		this.alias = alias;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public Geschlecht getGeschlecht() {
		return geschlecht;
	}

	public void setGeschlecht(Geschlecht geschlecht) {
		this.geschlecht = geschlecht;
	}

	public Set<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transaction> transactions) {
		this.transactions = transactions;
	}

	public Set<Transaction> addTransaction(Transaction transaction){
		this.transactions.add(transaction);
		return transactions;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		if(isDelete) this.active = Active.IN_ACTIVE;
		this.isDelete = isDelete;
	}

	public void addParticipatedClass(ParticipatedClass participatedClass){
		participatedClassruns.add(participatedClass);
	}

	public void setParticipatedClassruns(Set<ParticipatedClass> participatedClassruns) {
		this.participatedClassruns = participatedClassruns;
	}

	public Set<ParticipatedClass> getParticipatedClassruns() {
		return participatedClassruns;
	}


	public Subscription getSubscirption() {
		return subscirption;
	}

	public void setSubscirption(Subscription subscirption) {
		this.subscirption = subscirption;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", password=" + password + ", name=" + name + ", active="
				+ active + "]";
	}

}
