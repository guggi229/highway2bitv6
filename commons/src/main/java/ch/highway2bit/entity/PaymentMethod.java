/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PaymentMethod")
public class PaymentMethod implements Serializable {

	private static final long serialVersionUID = 6518502066819785152L;

	@Id
	@Column(name = "Symbol")
	private String symbol;

	@Column(name = "Name")
	private String name;

	/**
	 * Wo ist das Bild für dieses Symbol zu finden.
	 */
	@Column(name = "Path")
	private String path;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}


}
