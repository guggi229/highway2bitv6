/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import ch.highway2bit.model.global.TerminalRole;


@Entity
public class Terminal implements Serializable {
	private static final long serialVersionUID = -4759777633574668791L;

	@Id
	private String mac;

	@ManyToOne(fetch = FetchType.EAGER)
	private Room room;
	
	@Enumerated(EnumType.ORDINAL)
	private TerminalRole role;

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public TerminalRole getRole() {
		return role;
	}

	public void setRole(TerminalRole role) {
		this.role = role;
	}

}
