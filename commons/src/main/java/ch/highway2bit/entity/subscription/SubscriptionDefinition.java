/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity.subscription;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

import ch.highway2bit.utils.RegexConstants;
/**
 * Definiert die Daten eines Abonnements. So können gleiche Abos andere Preise haben. Beispiel Aktionen.
 * 
 * @author guggi229
 *
 */
@Entity
public class SubscriptionDefinition implements Serializable {

	private static final long serialVersionUID = 770155839058018036L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Pattern(regexp = RegexConstants.REGEX_NAME_INFO_ABO)
	private String name;

	private Long price;

	@Pattern(regexp = RegexConstants.REGEX_NAME_INFO_ABO)
	private String information;

	private SubscriptionTyp  subscriptionTyp;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	public String getInformation() {
		return information;
	}
	public void setInformation(String information) {
		this.information = information;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public SubscriptionTyp getSubscriptionTyp() {
		return subscriptionTyp;
	}
	public void setSubscriptionTyp(SubscriptionTyp subscriptionTyp) {
		this.subscriptionTyp = subscriptionTyp;
	}


}
