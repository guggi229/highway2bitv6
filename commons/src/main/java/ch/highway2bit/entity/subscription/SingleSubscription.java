/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
 *************************************************************************************/

package ch.highway2bit.entity.subscription;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


import ch.highway2bit.entity.ParticipatedClass;
import ch.highway2bit.exception.PaymentException;
import ch.highway2bit.model.ClassRunDuration;
import ch.highway2bit.model.alias.ChargedState;
import ch.highway2bit.model.global.Active;
import ch.highway2bit.model.pay.PayServiceInterface;
import ch.highway2bit.service.KursRunService;
import ch.highway2bit.service.UserService;
/**
 * Diese Entity hält die notwendigen Daten für einen Einzeleintritt.
 * @author guggi229
 *
 */
@Entity
@DiscriminatorValue("SINGLE")
public class SingleSubscription extends Subscription {

	private static final long serialVersionUID = 1566233757893926907L;

	/**
	 * Hier wird das Abonnement validiert. Bei einem Einzeleintritt gibt es nicht viel zu validieren.
	 */
	@Override
	public void validateSubscription(PayServiceInterface payService, UserService userService,KursRunService kursrunService,ParticipatedClass participatedClass) throws PaymentException {
		// Hier muss nichts validiert werden. Gleich zur Kasse!
		paySubscription(payService,userService,kursrunService, participatedClass);

		participatedClass.setCharged(ChargedState.OK);
		user.addParticipatedClass(participatedClass);
		userService.save(user);
	}

	/**
	 * Berechnet was ein Kurs kostet. Danach wird versucht, die Zahlung auszulösen.
	 */
	@Override
	protected void paySubscription(PayServiceInterface payService, UserService userService, KursRunService kursrunService,ParticipatedClass participatedClass) throws PaymentException {
		// Wie lange dauert der Kurs?
		// Dies müssen wir wissen, damit der richtige Preis basierend auf die Kursdauer bezahlt werden kann.
		ClassRunDuration duration = participatedClass.getKursrun().getDuration();
		double factor = (double) duration.getTimeInMin()/SECHZIG;
		LOG.info("Faktor: {} ", factor );
		LOG.info("Dauer: {}" , duration.getTimeInMin());
		double payingPrice = getSubscriptionDefinition().getPrice()*factor;
		try {
			payService.pay((long)payingPrice, user);
		}
		catch (PaymentException e) {
			// Besuchter Kurs wurde wohl nicht bezahlt. State auf Error. Muss von der Theke gprüft werden.
			participatedClass.setCharged(ChargedState.ERROR);
			user.addParticipatedClass(participatedClass);
			// User locken
			user.setActive(Active.IN_ACTIVE);
			userService.save(user);
			LOG.error("**************************************************");
			LOG.error("Fehler beim Kaufen. User {}. User wurde gesperrt: {} ",user.getEmail());
			LOG.error("**************************************************");

			throw new PaymentException();
		}
	}

}
