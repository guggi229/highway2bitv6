/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity.subscription;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.highway2bit.entity.ParticipatedClass;
import ch.highway2bit.entity.User;
import ch.highway2bit.exception.PaymentException;
import ch.highway2bit.model.pay.PayServiceInterface;
import ch.highway2bit.service.KursRunService;
import ch.highway2bit.service.UserService;


@Entity
@Inheritance
@DiscriminatorColumn(name="subscription_type")
public abstract class Subscription implements Serializable {

	protected static final Logger LOG = LoggerFactory.getLogger(Subscription.class);

	private static final long serialVersionUID = -5779363659219535021L;

	protected static final int SECHZIG = 60;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "subscriptionDefinition_id")
	protected SubscriptionDefinition subscriptionDefinition;

	@OneToOne(mappedBy = "subscirption")
	protected User user;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    public abstract void validateSubscription(PayServiceInterface payService, UserService userService, KursRunService kursrunService,ParticipatedClass participatedClass) throws PaymentException;

    protected abstract void paySubscription(PayServiceInterface payService, UserService userService, KursRunService kursrunService,ParticipatedClass participatedClass) throws PaymentException;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public SubscriptionDefinition getSubscriptionDefinition() {
		return subscriptionDefinition;
	}

	public void setSubscriptionDefinition(SubscriptionDefinition subscriptionDefinition) {
		this.subscriptionDefinition = subscriptionDefinition;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}




}
