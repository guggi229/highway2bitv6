/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity.subscription;
/**
 * Definiert den Typ des Abos. Bei einem späteren Release könnte man dies in die Abo Definition bauen.
 * 
 * @author guggi229
 *
 */
public enum SubscriptionTyp {

	ONE_YEAR(1), TEN_CARD(2), SINGLE_ENTRY(3);
	
	private int typeCode;

	SubscriptionTyp(int typeCode){
		this.typeCode=typeCode;
	}

	public int getTypeCode() {
		return typeCode;
	}


}
