/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
 *************************************************************************************/

package ch.highway2bit.entity.subscription;

import java.time.LocalDate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ch.highway2bit.entity.ParticipatedClass;
import ch.highway2bit.exception.PaymentException;
import ch.highway2bit.model.alias.ChargedState;
import ch.highway2bit.model.global.Active;
import ch.highway2bit.model.pay.PayServiceInterface;
import ch.highway2bit.service.KursRunService;
import ch.highway2bit.service.UserService;

@Entity
@DiscriminatorValue("YEAR")
public class YearSubscription extends Subscription {

	private static final long serialVersionUID = 6544524124042809002L;
	private static final Long YEAR=1L;

	private LocalDate startDate;

	private LocalDate endDate;


	public YearSubscription(LocalDate startDate) {
		this.startDate = startDate;
	}

	public YearSubscription() {

	}
	public LocalDate getStartDate() {
		return startDate;
	}

	public void setDate(LocalDate startDate) {
		this.startDate = startDate;
		this.endDate = startDate.plusYears(YEAR);
	}

	public LocalDate getEndDate() {
		return endDate;
	}
	/**
	 * Prüft, ob das Jahresabo gültig ist.
	 */
	@Override
	public void validateSubscription(PayServiceInterface payService, UserService userService,KursRunService kursrunService,ParticipatedClass participatedClass) throws PaymentException {
		LocalDate now = LocalDate.now();
		if (now.isBefore(endDate)&&now.isAfter(startDate)){
			participatedClass.setCharged(ChargedState.OK);
		}
		else {
			// Fachliche Exception. Abo nicht gültig
			participatedClass.setCharged(ChargedState.ERROR);
			user.addParticipatedClass(participatedClass);
			// User locken
			user.setActive(Active.IN_ACTIVE);
			userService.save(user);
			LOG.error("**************************************************");
			LOG.error("Fehler beim Validieren. User {}. User wurde gesperrt",user.getEmail());
			LOG.error("**************************************************");

			throw new PaymentException();
		}
		user.addParticipatedClass(participatedClass);
		userService.save(user);
	}


	@Override
	protected void paySubscription(PayServiceInterface payService, UserService userService, KursRunService kursrunService, ParticipatedClass participatedClass) throws PaymentException {
		// Jahres Abonnemente müssen an der Kasse bezahlt werden.

	}




}
