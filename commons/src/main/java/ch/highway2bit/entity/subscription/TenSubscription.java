/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity.subscription;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ch.highway2bit.entity.ParticipatedClass;
import ch.highway2bit.exception.PaymentException;
import ch.highway2bit.model.ClassRunDuration;
import ch.highway2bit.model.alias.ChargedState;
import ch.highway2bit.model.global.Active;
import ch.highway2bit.model.pay.PayServiceInterface;
import ch.highway2bit.service.KursRunService;
import ch.highway2bit.service.UserService;

@Entity
@DiscriminatorValue("TEN")
public class TenSubscription extends Subscription{

	private static final long serialVersionUID = 2058586839556848669L;
	private static final int ENTRIES = 10;

	private float availabeEntries;

	public TenSubscription(float availabeEntries){
		this.availabeEntries=availabeEntries;
	}
	public TenSubscription(){
		this.availabeEntries=ENTRIES;
	}

	public float getAvailabeEntries() {
		return availabeEntries;
	}

	public void setAvailabeEntries(int availabeEntries) {
		this.availabeEntries = availabeEntries;
	}

	/**
	 * Entwertet die 10erKarte und/oder kauft bei zu wenig Punkten eine neue 10er Karte.
	 *
	 *
	 */
	@Override
	public void validateSubscription(PayServiceInterface payService, UserService userService, KursRunService kursrunService, ParticipatedClass participatedClass) throws PaymentException {

		// Wie lange dauert der Kurs?
		// Dies müssen wir wissen, damit der richtige Preis basierend auf die Kursdauer bezahlt werden kann.
		ClassRunDuration duration = participatedClass.getKursrun().getDuration();
		float factor = (float) duration.getTimeInMin()/SECHZIG;
		LOG.info("Berechneter Faktor: {}", factor);
		/*
		 * Prüfe ob es auf der 10er Karte noch genügend Eintritte hat.
		 * Beispiel:
		 *
		 * Ein Kurs geht 30 Minuten. Somit kostet er einen halben Punkt auf einer 10er Karte.
		 * Der Faktor ist also 0.5
		 * Gibt es auf der 10er Karte vor der Methode noch 5 Punkte, kann dieser faktor abgezogen werden. Die 10erKarte wird also entwertet.
		 * Gibt es 0 Eintrotte auf der 10er Karte, so muss vorher eine neue Karte gekauft werden.
		 *
		 */
		LOG.info("Punktestand des Users {} ist {}", participatedClass.getUser().getEmail(), availabeEntries);
		if  (factor > availabeEntries){
			LOG.info("Punktestand reicht nicht!");
			paySubscription(payService, userService,  kursrunService,participatedClass);
		}


		// Hier wird die 10erKarte entwertet
		LOG.info("Entwerte 10er Karte.");
		availabeEntries=availabeEntries-factor;
		participatedClass.setCharged(ChargedState.OK);
		user.addParticipatedClass(participatedClass);
		userService.save(user);

	}

	/**
	 * Löst die Zahlung aus.
	 */
	@Override
	protected void paySubscription(PayServiceInterface payService, UserService userService, KursRunService kursrunService, ParticipatedClass participatedClass) throws PaymentException  {

		try {
			// Kauft eine neue 10erKarte und addiert den alten bestand hinzu.
			payService.pay(user.getSubscirption().getSubscriptionDefinition().getPrice(), user);
			availabeEntries = availabeEntries + ENTRIES;
			LOG.info("Neue Karte gekauft");

		} catch (PaymentException e) {
			// Besuchter Kurs wurde wohl nicht bezahlt. State auf Error. Muss von der Theke gprüft werden.
			participatedClass.setCharged(ChargedState.ERROR);
			user.addParticipatedClass(participatedClass);
			// User locken
			user.setActive(Active.IN_ACTIVE);
			userService.save(user);
			LOG.error("**************************************************");
			LOG.error("Fehler bei kaufen. User {}. User wurde gesperrt: {}",user.getEmail());
			LOG.error("**************************************************");
			throw new PaymentException();
		}
	}

}
