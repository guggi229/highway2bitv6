/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity;

import java.io.Serializable;
import java.util.Base64;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.Pattern;

import org.bson.types.Binary;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import ch.highway2bit.utils.RegexConstants;

@Entity
public class Kurs implements Serializable{

	private static final long serialVersionUID = 4520078711015239138L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="kurs_id")
	private Long id;

	@Column(name="name",unique=true)
	@Pattern(regexp = RegexConstants.REGEX_NAME,message = RegexConstants.TEXT_FORMAT_NAME_ERROR)

	private String kursName;

	@Column(name="image")
	@Lob
	private Binary image;

	@Column(name="deleted")
	private Boolean isDelete;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKursName() {
		return kursName;
	}

	public void setKursName(String kursName) {
		this.kursName = kursName;
	}

	public Binary getImage() {
		return image;
	}

	public void setImage(Binary image) {
		this.image = image;
	}

	public String getEncodedImage(){
		if (image!=null) return Base64.getEncoder().encodeToString(image.getData());
		else return null;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}



}
