/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import ch.highway2bit.model.ClassRunDuration;

@Entity
public class Kursrun implements Serializable {

	private static final long serialVersionUID = 6251969027619663610L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="kursrun_id")
	private Long id;

	@Column(name="date")
	@NotNull
	private LocalDate date;

	@Column(name="starttime")
	@NotNull
	private LocalTime startTime;

	@Column(name="amountcustomer")
	private Integer amountCustomer;

	@Column(name="duration")
	@NotNull
	private ClassRunDuration duration;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kurs_id")
	@NotNull
	private Kurs kurs;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "room_id")
	private Room room;

    @OneToMany(mappedBy = "kursrun")
    private Set<ParticipatedClass> participatedClassruns = new HashSet<>();

	@Column(name="deleted")
	private Boolean isDelete;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public Integer getAmountCustomer() {
		return amountCustomer;
	}

	public void setAmountCustomer(Integer amountCustomer) {
		this.amountCustomer = amountCustomer;
	}

	public ClassRunDuration getDuration() {
		return duration;
	}

	public void setDuration(ClassRunDuration duration) {
		this.duration = duration;
	}

	public Kurs getKurs() {
		return kurs;
	}

	public void setKurs(Kurs kurs) {
		this.kurs = kurs;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void addParticipatedClassrun (ParticipatedClass participatedClass){
		participatedClassruns.add(participatedClass);
	}

	public Set<ParticipatedClass> getParticipatedClassruns() {
		return participatedClassruns;
	}

	public void setParticipatedClassruns(Set<ParticipatedClass> participatedClassruns) {
		this.participatedClassruns = participatedClassruns;
	}



}
