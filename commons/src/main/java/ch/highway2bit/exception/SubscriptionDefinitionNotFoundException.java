/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.exception;

public class SubscriptionDefinitionNotFoundException extends Exception{

	private static final long serialVersionUID = 1138898794613060288L;

}
