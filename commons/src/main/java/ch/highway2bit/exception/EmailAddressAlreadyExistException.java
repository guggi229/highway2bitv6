/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.exception;


public class EmailAddressAlreadyExistException extends Exception{

	private static final long serialVersionUID = 1626765326235592603L;

}
