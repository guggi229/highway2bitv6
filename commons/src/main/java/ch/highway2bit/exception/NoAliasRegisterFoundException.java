/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.exception;

public class NoAliasRegisterFoundException extends Exception {
	private static final long serialVersionUID = -4311588854680354590L;

}
