/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.exception;

public class TokenNotFoundException extends Exception {
	private static final long serialVersionUID = -5086440186681167721L;

}
