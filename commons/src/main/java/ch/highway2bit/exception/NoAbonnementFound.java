/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.exception;


public class NoAbonnementFound extends Exception{

	private static final long serialVersionUID = 3998665604890162108L;

}
