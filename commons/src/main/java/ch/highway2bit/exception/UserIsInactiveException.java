/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.exception;

public class UserIsInactiveException extends Exception {

	private static final long serialVersionUID = -8685991317182958938L;

}
