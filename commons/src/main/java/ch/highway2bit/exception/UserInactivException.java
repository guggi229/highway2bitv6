/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.exception;

public class UserInactivException extends Exception
{
	private static final long serialVersionUID = 5942280731251619696L;

}
