/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.exception;


public class NfcCardNotFoundException extends Exception{

	private static final long serialVersionUID = 2624701516310810043L;

}
