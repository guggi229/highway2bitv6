/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.configuration.spring;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	/**
	 * Wurde der User eingeloggt, hat je nach Rolle der User eine andere Welcome Page. Diese werden hier defniert.
	 * 
	 */

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());

        if (roles.contains("ADMIN")) {
            httpServletResponse.sendRedirect("/admin/index");
        } else if (roles.contains("GROUP_MANAGER")) {
            httpServletResponse.sendRedirect("/groupmanager/index");
        }
        else if (roles.contains("CUSTOMER")) {
            httpServletResponse.sendRedirect("/customer/index");
        }
        else if (roles.contains("CUSTOMER_MANAGER")) {
            httpServletResponse.sendRedirect("/customermanager/index");
        }
    }
}
