/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.configuration.spring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@Order(1)
public class UserSecurityConfig extends WebSecurityConfigurerAdapter {

	// Navigations TO
	private static final String NAV_ROOT ="/";
	private static final String NAV_TOPIC ="/topic/**";
	private static final String NAV_APP ="/app/**";
	private static final String NAV_WEBSOCKET ="/websocket/**";
	private static final String NAV_INDEX ="/index";
	private static final String NAV_LOGIN ="/login";
	private static final String NAV_LOGOUT ="/logout";
	private static final String NAV_LOGIN_ERROR ="/login?error=true";
	private static final String NAV_ACCESS_DENIED ="/access-denied";
	private static final String NAV_TRAINER_DASHBOARD ="/dashboard/**";
	private static final String NAV_TEST ="/test/**";

	// Resources
	private static final String RES_APP_JS ="/app.js";
	private static final String RES_RESOURCES ="/resources/**";
	private static final String RES_STATIC ="/static/**";
	private static final String RES_CSS ="/css/**";
	private static final String RES_JS ="/js/**";
	private static final String RES_IMAGES ="/images/**";
	private static final String RES_WEBJARS ="/webjars/**";

	private static final String NAV_TO_REGISTRATION ="/registration";
	private static final String NAV_TO_REGISTER ="/register/**";
	private static final String NAV_TO_GLOBAL ="/global/**";
	private static final String NAV_TO_PASSWORT_CHANGE ="/customer/changepassword";
	private static final String NAV_TO_H2_CONSOLE ="/h2-console/**";
	private static final String NAV_TO_CONSOLE ="/console/**";
	private static final String NAV_TO_GROUPMANAGER ="/groupmanager/**";
	private static final String NAV_TO_CUSTOMERMANAGER ="/customermanager/**";
	private static final String NAV_TO_CUSTOMER ="/customer/**";
	private static final String NAV_TO_ALIAS ="/alias/**"; //
	private static final String NAV_TO_ADMIN ="/admin/**";
	private static final String NAV_TO_SIMULATOR ="/simulator/**";

	// Rollen
	private static final String ROLE_GROUPMANAGER ="GROUP_MANAGER";
	private static final String ROLE_ADMIN ="ADMIN";
	private static final String ROLE_CUSTOMERMANAGER ="CUSTOMER_MANAGER";
	private static final String ROLE_CUSTOMER ="CUSTOMER";

	// PARAMS
	private static final String PARAM_EMAIL ="email";
	private static final String PARAM_PASSWORD ="password"; // NOSONAR

	@Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.
                jdbcAuthentication()
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                .dataSource(dataSource)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.headers().frameOptions().disable();
        http.
                authorizeRequests()
                // Generelle Berechtigungen:
                .antMatchers(NAV_ROOT,NAV_TOPIC, NAV_APP, NAV_WEBSOCKET , NAV_INDEX ,RES_APP_JS,NAV_TRAINER_DASHBOARD,NAV_TEST).permitAll()
                .antMatchers(NAV_LOGIN).permitAll()
                .antMatchers(NAV_TO_REGISTRATION).permitAll()
                .antMatchers(NAV_TO_H2_CONSOLE).permitAll()
                .antMatchers(NAV_TO_CONSOLE).permitAll()
                .antMatchers(NAV_TO_REGISTER).permitAll()

                .antMatchers(NAV_TO_GROUPMANAGER).hasAuthority(ROLE_GROUPMANAGER)
                .antMatchers(NAV_TO_GLOBAL).hasAnyAuthority(ROLE_GROUPMANAGER,ROLE_CUSTOMER,ROLE_CUSTOMERMANAGER,ROLE_ADMIN)
                .antMatchers(NAV_TO_CUSTOMERMANAGER).hasAuthority(ROLE_CUSTOMERMANAGER)
                .antMatchers(NAV_TO_CUSTOMER).hasAuthority(ROLE_CUSTOMER)
                .antMatchers(NAV_TO_ALIAS).hasAuthority(ROLE_CUSTOMER)
                .antMatchers(NAV_TO_ADMIN,NAV_TO_SIMULATOR ).hasAuthority(ROLE_ADMIN).anyRequest()
                .authenticated().and().csrf().disable().formLogin()
                .loginPage(NAV_LOGIN).failureUrl(NAV_LOGIN_ERROR)
                .successHandler(authenticationSuccessHandler)
                .usernameParameter(PARAM_EMAIL)
                .passwordParameter(PARAM_PASSWORD)
                .and().logout()
                .logoutRequestMatcher(new AntPathRequestMatcher(NAV_LOGOUT))
                .logoutSuccessUrl(NAV_LOGIN).and().exceptionHandling()
                .accessDeniedPage(NAV_ACCESS_DENIED);

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers(RES_RESOURCES, RES_STATIC, RES_CSS, RES_JS, RES_IMAGES, RES_WEBJARS);
    }

}
