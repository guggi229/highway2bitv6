/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.listener;

import java.text.MessageFormat;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import ch.highway2bit.entity.User;
import ch.highway2bit.events.OnRegistrationCompleteEvent;
import ch.highway2bit.service.UserService;

/**
 * Konfiguriert den Listener beim Erstellen eines neuen Users.
 * Dieser löst beim Erstellen eines Kunden das Registrierungsmail aus.
 * 
 * @author guggi229
 *
 */

@Component
public class RegistrationListener implements  ApplicationListener<OnRegistrationCompleteEvent> {

	private static final Logger LOG = LoggerFactory.getLogger(RegistrationListener.class);

	// Navigiere
	public static final String NAV_REGISTER_LINK="/register/regitrationConfirm?token={0}";

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private UserService userService;


	@Override
	public void onApplicationEvent(OnRegistrationCompleteEvent event) {
		this.confirmRegistration(event);
	}

	private void confirmRegistration(OnRegistrationCompleteEvent event) {
		User user = event.getUser();
		String token = UUID.randomUUID().toString();
		userService.createVerificationToken(user, token);

		// Wir verwenden immer die gleiche Emailadresse. Natürlich sollte sonst die Adresse des Kunden hier stehen.
		String recipientAddress = "guggs2@bfh.ch";
		String subject = "Registration Confirmation";
		String confirmationUrl = MessageFormat.format(NAV_REGISTER_LINK, token);
		LOG.info("Sende Mail");
		SimpleMailMessage simpleEMailMessage = new SimpleMailMessage();
		simpleEMailMessage.setTo(recipientAddress);
		simpleEMailMessage.setSubject(subject);
		simpleEMailMessage.setFrom("info@group-fitness.ch");
		simpleEMailMessage.setText("http://localhost:8080" + confirmationUrl);
		javaMailSender.send(simpleEMailMessage);

	}
}
