/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit;

import java.io.File;
import java.nio.file.Files;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.User;
import ch.highway2bit.entity.subscription.SingleSubscription;
import ch.highway2bit.entity.subscription.Subscription;
import ch.highway2bit.entity.subscription.SubscriptionDefinition;
import ch.highway2bit.entity.subscription.SubscriptionTyp;
import ch.highway2bit.entity.subscription.TenSubscription;
import ch.highway2bit.entity.subscription.YearSubscription;
import ch.highway2bit.model.ClassRunDuration;
import ch.highway2bit.model.global.Active;
import ch.highway2bit.model.global.Currency;
import ch.highway2bit.entity.Alias;
import ch.highway2bit.entity.Kurs;
import ch.highway2bit.entity.Kursrun;
import ch.highway2bit.entity.NfcCard;
import ch.highway2bit.entity.ParticipatedClass;
import ch.highway2bit.entity.PaymentMethod;
import ch.highway2bit.entity.Role;
import ch.highway2bit.entity.Room;
import ch.highway2bit.entity.Terminal;
import ch.highway2bit.repository.KursRepository;
import ch.highway2bit.repository.KursrunRepository;
import ch.highway2bit.repository.NfcCardRepository;
import ch.highway2bit.repository.PaymentMethodRepository;
import ch.highway2bit.repository.RoleRepository;
import ch.highway2bit.repository.RoomRepository;
import ch.highway2bit.repository.TerminalRepository;
import ch.highway2bit.repository.UserRepository;
import ch.highway2bit.service.ParticipatedClassService;
import ch.highway2bit.service.PaymentMethodService;
import ch.highway2bit.service.SubscirptionService;
import ch.highway2bit.service.SubscriptionDefinitionService;
import ch.highway2bit.service.UserService;

import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.h2.tools.Server;

/**
 * Dies ist die Hauptklasse. Für das dev-Profile, können diese Daten behalten werden. Sie setzten ein paar Daten in die Datenbank. Somit kann
 * sofort damit gearbeitet werden.
 * 
 * @author guggi229
 *
 */

@SpringBootApplication
@ServletComponentScan
@EnableScheduling
public class Application {



	@Autowired
	private UserService userService;

	@Autowired
	private PaymentMethodService paymentMethodService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private ParticipatedClassService participatedClassService;

	@Autowired
	private KursrunRepository kursrunRepository;

	@Autowired
	private RoomRepository roomRepository;

	@Autowired
	private NfcCardRepository nfcCardRepository;

	@Autowired
	private SubscirptionService subscirptionService;

	@Autowired
	private SubscriptionDefinitionService subscriptionDefinitionService;

	@Autowired
	private TerminalRepository terminalRepository;

	@Autowired
	private KursRepository kursRepository;

	@Autowired
	private PaymentMethodRepository paymentMethodRepository;

	@Autowired
	private ResourceLoader resourceLoader;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	private static final String BACK_IMAGE_NAME="static/images/back.PNG";
	private static final String YOGA_IMAGE_NAME="static/images/yoga.PNG";
	private static final String PILATES_IMAGE_NAME="static/images/pilates.PNG";
	private static final String PUMP_IMAGE_NAME="static/images/pump.PNG";
	private static final String NO_CLASS_IMAGE_NAME="static/images/noclassoic.PNG";

	// private static final String STOP_IMAGE_NAME="static/images/stop.PNG";

	@Bean
	@Transactional
	public CommandLineRunner init( UserRepository userRepository, RoleRepository roleRepository, KursRepository kursRepository, KursrunRepository kursrunRepository) {
		return (args) -> {

			// ********************************************************

			// Ein paar Abo-Defintionen vorbereiten

			// ********************************************************

			SubscriptionDefinition yearDefinition = new SubscriptionDefinition();
			yearDefinition.setInformation("Jahres Abonnement gültig ab 2017");
			yearDefinition.setPrice(98000L);
			yearDefinition.setName("Jahres Abonnement");
			yearDefinition.setSubscriptionTyp(SubscriptionTyp.ONE_YEAR);
			yearDefinition = subscriptionDefinitionService.save(yearDefinition);
		

			SubscriptionDefinition tenDefinition = new SubscriptionDefinition();
			tenDefinition.setInformation("10er Abonnement gültig ab 2017");
			tenDefinition.setPrice(22000L);
			tenDefinition.setName("10er Abonnement");
			tenDefinition.setSubscriptionTyp(SubscriptionTyp.TEN_CARD);
			tenDefinition = subscriptionDefinitionService.save(tenDefinition);

			SubscriptionDefinition singleDefinition = new SubscriptionDefinition();
			singleDefinition.setInformation("10er Abonnement gültig ab 2017");
			singleDefinition.setPrice(1900L);
			singleDefinition.setName("Einzel Eintritt");
			singleDefinition.setSubscriptionTyp(SubscriptionTyp.SINGLE_ENTRY);
			singleDefinition = subscriptionDefinitionService.save(singleDefinition);

			// ********************************************************

			// Ein paar Payment Methode vorbereiten

			// ********************************************************

			PaymentMethod payment = new PaymentMethod();
			payment.setSymbol("VIS");
			payment.setName("VISA");
			payment.setPath("/images/cardsymbols/visacard.PNG");
			paymentMethodRepository.save(payment);



			// ********************************************************

			// Ein paar Rollen vorbereiten

			// ********************************************************

			Role admin = new Role();
			admin.setRole("ADMIN");
			roleRepository.save(admin);

			Role customer = new Role();
			customer.setRole("CUSTOMER");
			roleRepository.save(customer);

			Role cmanager = new Role();
			cmanager.setRole("CUSTOMER_MANAGER");
			roleRepository.save(cmanager);

			Role gmanager = new Role();
			gmanager.setRole("GROUP_MANAGER");
			roleRepository.save(gmanager);

			Role trainer = new Role();
			trainer.setRole("TRAINER");
			trainer = roleRepository.save(trainer);

			// ********************************************************

			// Verschlüsseletes Passwort auf die Test User setzten.

			// ********************************************************
			User trainer1 = new User();
			trainer1.setEmail("trainer1@bfh.ch");
			trainer1.setPassword(bCryptPasswordEncoder.encode("123"));
			trainer1.setName("trainer1");
			trainer1.setVorname("Thomas");
			trainer1.setActive(Active.ACTIVE);
			trainer1.setIsDelete(false);
			trainer1.addRole(trainer);
			userService.save(trainer1);


			// ********************************************************

			User trainer2 = new User();
			trainer2.setEmail("trainer2@bfh.ch");
			trainer2.setPassword(bCryptPasswordEncoder.encode("123"));
			trainer2.setName("trainer2");
			trainer2.setVorname("Thomas");
			trainer2.setActive(Active.ACTIVE);
			trainer2.setIsDelete(false);
			trainer2.addRole(trainer);
			userService.save(trainer2);


			// ********************************************************

			User cmanager1 =  new User();
			cmanager1.setEmail("cmanager1@bfh.ch");
			cmanager1.setPassword(bCryptPasswordEncoder.encode("123"));
			cmanager1.setName("cmanager1");
			cmanager1.setVorname("Maria");
			cmanager1.setActive(Active.ACTIVE);
			cmanager1.setIsDelete(false);
			cmanager1.addRole(cmanager);
			userService.save(cmanager1);

			// ********************************************************

			Subscription single1 = new SingleSubscription();
			single1.setSubscriptionDefinition(singleDefinition);


			NfcCard nfcRoleCard = new NfcCard();
			nfcRoleCard.setId(bytesToHex(new byte[]{(byte)0xBC, (byte)0x67,(byte)0xEF,(byte)0x2B}));
			User customer1 = new User();
			customer1.setIsDelete(false);
			customer1.setEmail("customer1@bfh.ch");
			customer1.setPassword(bCryptPasswordEncoder.encode("123"));
			customer1.setNfcCard(nfcRoleCard);
			customer1.setActive(Active.ACTIVE);
			customer1.setName("customer1");
			customer1.setVorname("Thomas");
			Alias alias = new Alias();
			alias.setAliasCc("16421623742496390");
			alias.setCurrency(Currency.CHF);
			alias.setExpiryMonth("12");
			alias.setExpiryYear("21");
			alias.setMaskedCreditCardNumber("400360xxxxxx0006");
			PaymentMethod payMethod = paymentMethodService.findBySymbol("VIS");
			alias.setPmethode(payMethod);
			customer1.setAlias(alias);
			customer1.addRole(customer);

			//single1 = subscirptionService.save(single1);
			single1.setUser(customer1);

			customer1.setSubscirption(single1);
			userService.save(customer1);

			// ********************************************************

			User admin1 =  new User();
			admin1.setEmail("admin@bfh.ch");
			admin1.setPassword(bCryptPasswordEncoder.encode("123"));
			admin1.setName("admin1");
			admin1.setVorname("Peter");
			admin1.setActive(Active.ACTIVE);
			admin1.setIsDelete(false);
			admin1.addRole(admin);
			userService.save(admin1);


			// ********************************************************

			User gmanager1 =  new User();
			gmanager1.setEmail("gmanager1@bfh.ch");
			gmanager1.setPassword(bCryptPasswordEncoder.encode("123"));
			gmanager1.setActive(Active.ACTIVE);
			gmanager1.setIsDelete(false);
			gmanager1.addRole(gmanager);
			userService.save(gmanager1);


			// ********************************************************

			User gmanager2 =  new User();
			gmanager2.setEmail("gmanager2@bfh.ch");
			gmanager2.setPassword(bCryptPasswordEncoder.encode("123"));
			gmanager2.setActive(Active.ACTIVE);
			gmanager2.setIsDelete(false);
			gmanager2.addRole(gmanager);
			userService.save(gmanager2);



			// ********************************************************

			TenSubscription ten1 = new TenSubscription();
			ten1.setSubscriptionDefinition(tenDefinition);
			ten1.setAvailabeEntries(2);
			//subscirptionService.save(ten1);

			NfcCard nfcRoleCard2 = new NfcCard();
			nfcRoleCard2.setId(bytesToHex(new byte[]{(byte)0x82, (byte)0xE3,(byte)0xEE,(byte)0x2B}));

			User customer2 =  new User();
			customer2.setEmail("customer2@bfh.ch");
			customer2.setPassword(bCryptPasswordEncoder.encode("123"));
			customer2.setActive(Active.ACTIVE);
			customer2.setName("Wasser");
			customer2.setVorname("Nicole");
			customer2.setIsDelete(false);
			customer2.addRole(customer);
			userService.save(customer2);

			customer2.setNfcCard(nfcRoleCard2);

			Alias alias2 = new Alias();
			alias2.setAliasCc("12920633418257406");
			alias2.setCurrency(Currency.CHF);
			alias2.setExpiryMonth("12");
			alias2.setExpiryYear("21");
			alias2.setMaskedCreditCardNumber("444409xxxxxx0103");
			PaymentMethod payMethod2 = paymentMethodService.findBySymbol("VIS");
			alias2.setPmethode(payMethod2);
			customer2.setAlias(alias2);
			customer2.setSubscirption(ten1);
			userService.save(customer2);

			// ********************************************************

			User customer3 =  new User();
			customer3.setEmail("customer3@bfh.ch");
			customer3.setPassword(bCryptPasswordEncoder.encode("123"));
			customer3.setActive(Active.ACTIVE);
			customer3.setName("Teller");
			customer3.setVorname("Jan");
			customer3.setIsDelete(false);
			customer3.addRole(customer);

			YearSubscription year1 = new YearSubscription();
			year1.setSubscriptionDefinition(yearDefinition);
			year1.setDate(LocalDate.now().minusMonths(6l));
			//year1 = (YearSubscription) subscirptionService.save(year1);

			NfcCard nfcRoleCard3 = new NfcCard();
			nfcRoleCard3.setId(bytesToHex(new byte[]{(byte)0xCA, (byte)0xAD,(byte)0x35,(byte)0x3D}));

			customer3.setNfcCard(nfcRoleCard3);
			customer3.setActive(Active.ACTIVE);
			customer3.setSubscirption(year1);
			userService.save(customer3);

			// ********************************************************

			//NfcCard nfcRoleCard4 = new NfcCard();
			//nfcRoleCard4.setId(bytesToHex(new byte[]{(byte)0xBF, (byte)0xC1,(byte)0x37,(byte)0x3D}));

			User customer4 =  new User();
			customer4.setEmail("customer4@bfh.ch");
			customer4.setPassword(bCryptPasswordEncoder.encode("123"));
			customer4.setActive(Active.ACTIVE);
			customer4.setIsDelete(false);
			customer4.addRole(customer);
			userService.save(customer4);

			//	customer4.setNfcCard(nfcRoleCard4);

			Alias alias3 = new Alias();
			alias3.setAliasCc("70119122433810042");
			alias3.setCurrency(Currency.CHF);
			alias3.setExpiryMonth("12");
			alias3.setExpiryYear("21");
			alias3.setMaskedCreditCardNumber("424242xxxxxx4242");
			PaymentMethod payMethod3 = paymentMethodService.findBySymbol("VIS");
			alias3.setPmethode(payMethod3);
			customer4.setAlias(alias3);
			userService.save(customer4);

			/**
			 * Ein paar Test Kurse setzten inkl. Bild
			 *
			 */

			ClassLoader classLoader = ClassLoader.getSystemClassLoader();

			
			File noClassImage = new File(classLoader.getResource(NO_CLASS_IMAGE_NAME).getFile());
			File backImgage = new File(classLoader.getResource(BACK_IMAGE_NAME).getFile());
			File yogaImgage = new File(classLoader.getResource(YOGA_IMAGE_NAME).getFile());
			File pilatesImgage = new File(classLoader.getResource(PILATES_IMAGE_NAME).getFile());

			File pumpImgage = new File(classLoader.getResource(PUMP_IMAGE_NAME).getFile());

			//File stopImage = new File(classLoader.getResource(STOP_IMAGE_NAME).getFile());

			// byte[] defaultContent = Files.readAllBytes(defaultImgage.toPath());
			byte[] backContent = Files.readAllBytes(backImgage.toPath());
			byte[] yogaContent = Files.readAllBytes(yogaImgage.toPath());
			byte[] pilatesContent = Files.readAllBytes(pilatesImgage.toPath());
			byte[] pumpContent = Files.readAllBytes(pumpImgage.toPath());
			byte[] noContent = Files.readAllBytes(noClassImage.toPath());
			//	byte[] stopContent = Files.readAllBytes(stopImage.toPath());

			Kurs noClass = new Kurs();
			noClass.setKursName("Kein Kurs");
			noClass.setIsDelete(false);

			Kurs yogaKurs = new Kurs();
			yogaKurs.setKursName("Yoga");
			yogaKurs.setIsDelete(false);

			Kurs backKurs = new Kurs();
			backKurs.setKursName("Rücken");
			backKurs.setIsDelete(false);

			Kurs pilatesKurs = new Kurs();
			pilatesKurs.setKursName("Pilates");
			pilatesKurs.setIsDelete(false);

			Kurs pumpKurs = new Kurs();
			pumpKurs.setKursName("Pump");
			pumpKurs.setIsDelete(false);


			//	noKurs.setImage((new Binary(BsonBinarySubType.BINARY, stopContent)));
			yogaKurs.setImage((new Binary(BsonBinarySubType.BINARY, yogaContent)));
			backKurs.setImage((new Binary(BsonBinarySubType.BINARY, backContent)));
			pilatesKurs.setImage((new Binary(BsonBinarySubType.BINARY, pilatesContent)));
			pumpKurs.setImage((new Binary(BsonBinarySubType.BINARY, pumpContent)));
			noClass.setImage((new Binary(BsonBinarySubType.BINARY, noContent)));

			//	kursRepository.save(noKurs);
			kursRepository.save(yogaKurs);
			kursRepository.save(backKurs);
			kursRepository.save(pilatesKurs);
			kursRepository.save(pumpKurs);
			kursRepository.save(noClass);

			setUpRooms();
			setUpTerminals();
			setUpClassRuns();
		};
	}


	public void setUpTerminals(){
		Terminal terminal1 = new Terminal();
		terminal1.setMac("02-00-4C-4F-4F-50");
		Optional<Room> room1Opt = roomRepository.findById(1L);

		if (room1Opt.isPresent()){
			terminal1.setRoom(room1Opt.get());
		}
		terminalRepository.save(terminal1);


		Terminal terminal2 = new Terminal();
		terminal2.setMac("02-00-4C-4F-4F-51");
		Optional<Room> room2Opt = roomRepository.findById(2L);
		if (room2Opt.isPresent()){
			terminal2.setRoom(room2Opt.get());
		}
		terminalRepository.save(terminal2);
	}

	public void setUpRooms(){
		Room roomBlau = new Room();
		roomBlau.setIsDelete(false);
		roomBlau.setName("blau");

		NfcCard nfcCard = new NfcCard();
		nfcCard.setId(bytesToHex(new byte[]{(byte)0x51, (byte)0x6b,(byte)0x37,(byte)0x3d}));

		nfcCard.setRoom(roomBlau);
		roomBlau.addNfcCards(nfcCard);
		roomRepository.save(roomBlau);

		Room roomrot = new Room();
		roomrot.setName("rot");
		roomrot.setIsDelete(false);

		NfcCard nfcCard2 = new NfcCard();
		nfcCard2.setId(bytesToHex(new byte[]{(byte)0xb1, (byte)0xda,(byte)0x35,(byte)0x3d}));

		nfcCard2.setRoom(roomrot);
		roomrot.addNfcCards(nfcCard2);
		roomRepository.save(roomrot);

	}

	public void setNFCRoomCard(){

		Optional<Room> optRoom = roomRepository.findById(1L);
		Optional<NfcCard> optNFCRoleCard = nfcCardRepository.findById(bytesToHex(new byte[]{(byte)0xdd, (byte)0xE5,(byte)0x9B,(byte)0x03}));

		if(optRoom.isPresent() && optNFCRoleCard.isPresent()){
			Room room = optRoom.get();
			NfcCard nfcRoleCard = optNFCRoleCard.get();
			nfcRoleCard.setRoom(room);
			room.addNfcCards(nfcRoleCard);
			room = roomRepository.save(room);
			System.out.println("Raum:" + room.getName());
			Set<NfcCard> NfcCards = room.getNfcCards();

			for (NfcCard nfcCard : NfcCards) {
				System.out.println(nfcCard.getId());
			}
		}

		Optional<NfcCard> optNFCRoleCard2 = nfcCardRepository.findById(bytesToHex(new byte[]{(byte)0xdd, (byte)0xE5,(byte)0x9B,(byte)0x03}));
		if(optNFCRoleCard2.isPresent()){
			System.out.println("Raum Nanme: " +optNFCRoleCard2.get().getRoom().getName());
		}


	}

	/**
	 * Hier werden diverse Kunden in einen Kursrun gebucht.
	 */

	public void setUpKursrun() {
		Optional<Kursrun> kursrunOpt2 = kursrunRepository.findById(2L);
		for (Long i = 20L; i < 31L; i++) {
			Optional<User> userOPt = userService.findById(i);
			if (userOPt.isPresent()&&kursrunOpt2.isPresent()) {
				User user = userOPt.get();
				ParticipatedClass participatedClass = new ParticipatedClass();
				participatedClass.setKursrun(kursrunOpt2.get());
				participatedClass.setUser(user);
				participatedClassService.save(participatedClass);
			}
		}

		Optional<Kursrun> kursrunOpt1 = kursrunRepository.findById(1L);

		for (Long i = 14L; i < 18L; i++) {
			Optional<User> userOPt = userService.findById(i);
			if (userOPt.isPresent()&&kursrunOpt1.isPresent()) {
				User user = userOPt.get();
				ParticipatedClass participatedClass = new ParticipatedClass();
				participatedClass.setKursrun(kursrunOpt1.get());
				participatedClass.setUser(user);
				participatedClassService.save(participatedClass);
			}
		}

	}

	public void setUpClassRuns(){
		Optional<Kurs> kurs1Opt = kursRepository.findById(1L);
		Optional<Kurs> kurs2Opt = kursRepository.findById(2L);
		//	Optional<Kurs> kurs3Opt = kursRepository.findById(3L);
		Optional<Kurs> kurs4Opt = kursRepository.findById(4L);

		Optional<Room> room1Opt = roomRepository.findById(1L);
		Optional<Room> room2Opt = roomRepository.findById(2L);

		// Kursraum 1
		Kursrun kr1 = new Kursrun();
		kr1.setDuration(ClassRunDuration.DREISSIG_MINUTEN);
		kr1.setRoom(room1Opt.get());
		kr1.setKurs(kurs1Opt.get());
		kr1.setIsDelete(false);
		kr1.setStartTime(LocalTime.now());
		kr1.setDate(LocalDate.now());


		Kursrun kr3 = new Kursrun();
		kr3.setDuration(ClassRunDuration.SECHZIG_MINUTEN);
		kr3.setRoom(room1Opt.get());
		kr3.setKurs(kurs2Opt.get());
		kr3.setIsDelete(false);
		kr3.setStartTime(LocalTime.now().plusHours(1));
		kr3.setDate(LocalDate.now());

		Kursrun kr4 = new Kursrun();
		kr4.setDuration(ClassRunDuration.NEUNZIG_MINUTEN);
		kr4.setRoom(room1Opt.get());
		kr4.setKurs(kurs4Opt.get());
		kr4.setStartTime(LocalTime.now().plusHours(2));
		kr4.setIsDelete(false);
		kr4.setDate(LocalDate.now());

		// Kursraum 2
		Kursrun kr2 = new Kursrun();
		kr2.setDuration(ClassRunDuration.SECHZIG_MINUTEN);
		kr2.setRoom(room2Opt.get());
		kr2.setKurs(kurs2Opt.get());
		kr2.setIsDelete(false);
		kr2.setStartTime(LocalTime.now());
		kr2.setDate(LocalDate.now());

		kursrunRepository.save(kr1);
		kursrunRepository.save(kr2);
		kursrunRepository.save(kr3);
		kursrunRepository.save(kr4);


	}



	/**
	 * H2 konfigurieren, damit mehrere Spring-Boot Applikationen darauf zugreifen können.
	 * @return
	 * @throws SQLException
	 */
	@Bean(initMethod = "start", destroyMethod = "stop")
	public Server inMemoryH2DatabaseaServer() throws SQLException {
		return Server.createTcpServer(
				"-tcp", "-tcpAllowOthers", "-tcpPort", "9090");
	}

	private static String bytesToHex(byte[] hashInBytes) {
		StringBuilder sb = new StringBuilder();
		for (byte b : hashInBytes) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}
}
