/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.bind.JAXBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ch.highway2bit.datatrans.xsd.AuthorizationService;
import ch.highway2bit.entity.Transaction;
import ch.highway2bit.entity.User;
import ch.highway2bit.exception.NoAliasRegisterFoundException;
import ch.highway2bit.exception.PaymentException;
import ch.highway2bit.exception.UserInactivException;
import ch.highway2bit.mode.transaction.TransactionState;
import ch.highway2bit.mode.transaction.TransactionTyp;
import ch.highway2bit.model.global.XmlConstans;
import ch.highway2bit.model.pay.PayResponse;
import ch.highway2bit.model.pay.PayResponseConstans;
import ch.highway2bit.model.pay.PayServiceInterface;
import ch.highway2bit.repository.PayResponseRepository;

/**
 * Ist für das Auslösen der Zahlung zuständig.
 *
 * @author guggisberste
 *
 */
@Service
@Transactional
public class PayService implements PayServiceInterface {

	private static final Logger LOG = LoggerFactory.getLogger(PayService.class);

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private PayResponseRepository payResponseRepository;

	@Autowired
	private XmlService xmlService;

	@Value("${datatrans.pay.url}")
	private String payUrl;

	@Value("${datatrans.basic.auth}")
	private String basicAuth;

	private static final String POST = "POST";
	private static final int TIMEOUT = 20000;


	/**
	 * Löst die Transaktion zu Datatrans aus.
	 * @param betrag
	 * @param user
	 * @return
	 * @throws PaymentException
	 * @throws JAXBException
	 * @throws RefNoAlreadyExistException
	 * @throws NoAliasRegisterFoundException
	 * @throws UserInactivException
	 * @throws IOException
	 */

	public void pay(Long betrag, User user) throws PaymentException {
		LOG.info("Datatrans URL: {}", payUrl);

		// Generiere eine Eindeutige Referenz Nummer um die Transaktion wieder zu finden.
		Transaction transaction = transactionService.createTransaction(TransactionTyp.PAY_WITH_ALIAS);

		String request;
		String response;
		try {
			request = xmlService.generateRequestXml(user.getEmail(), betrag);

			URL url = new URL(payUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setConnectTimeout(TIMEOUT);
			connection.setReadTimeout(TIMEOUT);

			connection.setDoOutput(true);

			connection.setUseCaches(true);
			connection.setRequestMethod(POST);

			// Setze Headers
			connection.setRequestProperty(XmlConstans.ACCEPT, XmlConstans.ACCEPT_VALUE);
			connection.setRequestProperty(XmlConstans.CONTENT_TYPE, XmlConstans.CONTENT_TYPE_VALUE);
			connection.setRequestProperty(XmlConstans.AUTHORIZATION, basicAuth);

			// XML erstellen
			OutputStream outputStream = connection.getOutputStream();
			byte[] b = request.getBytes(XmlConstans.ENCODING);
			outputStream.write(b);
			outputStream.flush();
			outputStream.close();

			// lsesen des XMLs
			InputStream inputStream = connection.getInputStream();
			byte[] res = new byte[XmlConstans.STREAM_SIZE];
			int i = 0;
			StringBuilder strinBuilderResponse = new StringBuilder();
			while ((i = inputStream.read(res)) != -1) {
				strinBuilderResponse.append(new String(res, 0, i));
			}
			inputStream.close();
			response = strinBuilderResponse.toString();
			LOG.info("Response={} ", response);
		} catch (JAXBException  | UserInactivException | IOException | NoAliasRegisterFoundException e) {
			LOG.error("Fehler:", e);
			throw new PaymentException();
		}

		// Transaktion buchen
		transaction.setState(TransactionState.GEBUCHT);
		transaction.setUser(user);

		// Daten in das von Datatrans zur Verfügung gestellte Objekt mappen
		PayResponse payResponse = new PayResponse();
		AuthorizationService authorizationService = new AuthorizationService();
		authorizationService = xmlService.generateObjectFromXML(response);
		payResponse.setResponseCode(authorizationService.getBody().getTransaction().getResponse().getResponseCode());
		payResponse.setResponseMessage(authorizationService.getBody().getTransaction().getResponse().getResponseMessage());
		payResponse.setUppTransactionId(authorizationService.getBody().getTransaction().getResponse().getUppTransactionId());
		payResponse.setAuthorizationCode(authorizationService.getBody().getTransaction().getResponse().getAuthorizationCode());
		payResponse.setAcqAuthorizationCode(authorizationService.getBody().getTransaction().getResponse().getAcqAuthorizationCode());
		payResponse.setAliasCC(authorizationService.getBody().getTransaction().getResponse().getMaskedCC());
		// Speichert die Antwort von Datatrans
		payResponse= payResponseRepository.save(payResponse);

		// Fehlerfall:
		if (!payResponse.getResponseCode().equals(PayResponseConstans.RESPONSE_CODE_FROM_DATATRANS)){
			LOG.error("Fehler von Datatrans");
			transaction.setState(TransactionState.ERROR);
			transactionService.save(transaction);
		}

	}






}
