/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.Alias;
import ch.highway2bit.entity.PaymentMethod;
import ch.highway2bit.entity.Transaction;
import ch.highway2bit.entity.User;
import ch.highway2bit.exception.CanceledAliasRegisterException;
import ch.highway2bit.exception.DatatransException;
import ch.highway2bit.exception.PaymentMethodNotSupportedException;
import ch.highway2bit.exception.UnknownAliasregisterException;
import ch.highway2bit.mode.transaction.TransactionState;
import ch.highway2bit.mode.transaction.TransactionTyp;
import ch.highway2bit.model.alias.AliasConstans;
import ch.highway2bit.model.alias.AliasRegisterRequestModel;
import ch.highway2bit.model.alias.AliasRegisterResponeModel;
import ch.highway2bit.model.alias.DataTransState;
import ch.highway2bit.model.alias.UppReturnTarget;
import ch.highway2bit.model.global.Currency;
import ch.highway2bit.repository.TransactionRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Diese Klasse generiert die notwendgen Daten, um einen Alias zu registrieren.
 * 
 * @author guggi229
 *
 */
@Service
@Transactional
public class AliasService {

	private AliasRegisterRequestModel aliasRegisterTransactionModel;
	private static final Logger LOG = LoggerFactory.getLogger(AliasService.class);

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private UserService userService;

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private PaymentMethodService paymentMethodService;

	private final static String NULL = "0";

	@Value("${datatrans.merchantId_3d_secure}")
	private String merchantId_3d;

	@Value("${datatrans.sign.alias}")
	private Long sign;

	@Value("${datatrans.alias.create.url}")
	private String startAliasUrl;

	@Value("${datatrans.alias.success.url}")
	private String successUrl;

	@Value("${datatrans.alias.reg.error.url}")
	private String errorUrl;

	@Value("${datatrans.alias.reg.cancel.url}")
	private String cancelUrl;

	public String getRequestUrl(){
		return startAliasUrl;
	}

	/**
	 * Diese Methode generiert das Alias Model. Dieses wird dann zu Datatrans gesendet um einen Alias zu erzeugen.
	 * 
	 * @param principal
	 * @return
	 */
	public AliasRegisterRequestModel generateAliasModel(Principal principal) {
		LOG.info("Generiere AliasRequest");
		aliasRegisterTransactionModel = new AliasRegisterRequestModel();
		aliasRegisterTransactionModel.setMerchantId(merchantId_3d);
		aliasRegisterTransactionModel.setAmount(new BigDecimal(NULL));
		aliasRegisterTransactionModel.setCurrency(Currency.CHF);
		aliasRegisterTransactionModel.setRefNo(transactionService.createTransaction(TransactionTyp.CREATE_ALIAS).getRefNo());
		aliasRegisterTransactionModel.setSign(sign);
		aliasRegisterTransactionModel.setSuccessUrl(successUrl);
		aliasRegisterTransactionModel.setErrorUrl(errorUrl);
		aliasRegisterTransactionModel.setCancelUrl(cancelUrl);
		aliasRegisterTransactionModel.setUppWebResponseMethod(AliasConstans.UPP_WEB_RESPONSE_METHODE);
		aliasRegisterTransactionModel.setTheme(AliasConstans.THEME);
		aliasRegisterTransactionModel.setVersion(AliasConstans.VERSION);
		aliasRegisterTransactionModel.setUppReturnTarget(UppReturnTarget._top);
		aliasRegisterTransactionModel.setUseAlias(AliasConstans.USE_ALIAS);
		return aliasRegisterTransactionModel;
	}
	/**
	 * Hier wird der Alias gelöscht.
	 * @param principal
	 */
	public void deleteAlias(Principal principal)  {
		User user =userService.findUserByEmail(principal.getName());
		user.setAlias(null);
		Transaction transaction = transactionService.createTransaction(TransactionTyp.DELETE_ALIAS);
		transaction.setState(TransactionState.OK);
		user.addTransaction(transaction);
		transaction.setUser(user);

		userService.save(user);
		transactionRepository.save(transaction);

	}
/**
 * Die Daten von Datatrans werden hier gespeichert. Es handelt sich dabei um die Antwort auf den Alias Request.
 * Dieser wird vorher noch geprüft.
 * 
 * @param aliasRegisterResponseModel
 * @param principal
 * @throws UnknownAliasregisterException
 * @throws CanceledAliasRegisterException
 * @throws DatatransException
 * @throws PaymentMethodNotSupportedException
 */
	public void saveAlias(AliasRegisterResponeModel aliasRegisterResponseModel, Principal principal ) throws UnknownAliasregisterException, CanceledAliasRegisterException, DatatransException, PaymentMethodNotSupportedException{

		if (aliasRegisterResponseModel== null) {
			LOG.error("Antwort von Datatrans leer");
			throw new UnknownAliasregisterException();
		}
		else if(aliasRegisterResponseModel.getStatus().equalsIgnoreCase(DataTransState.SUCCESS.name())){
			Alias alias = new Alias();
			Optional<Transaction> opt = transactionRepository.findById(aliasRegisterResponseModel.getRefno());
			User user =userService.findUserByEmail(principal.getName());
			PaymentMethod paymentMethode = paymentMethodService.getPaymentMethodeBySymbol(aliasRegisterResponseModel.getPmethod());
			Currency currency = Currency.valueOf(aliasRegisterResponseModel.getCurrency());
			alias.setAliasCc(aliasRegisterResponseModel.getAliasCC());
			alias.setCurrency(currency);
			alias.setExpiryMonth(aliasRegisterResponseModel.getExpm());
			alias.setExpiryYear(aliasRegisterResponseModel.getExpy());
			alias.setMaskedCreditCardNumber(aliasRegisterResponseModel.getMaskedCC());

			alias.setPmethode(paymentMethode);
			//alias.setPmethode(aliasRegisterResponseModel.getPmethod());
			user.setAlias(alias);

			if (opt.isPresent()){
				Transaction transaction = opt.get();
				transaction.setCurrency(currency);
				transaction.setState(TransactionState.GEBUCHT);
				transaction.setUser(user);
				transactionRepository.save(transaction);
				userService.save(user);
				LOG.info("Alias gespeichert");
			}
			else {
				LOG.error("Transactin von Datatrans ungültig");
				throw new UnknownAliasregisterException();
			}

		}
		else if(aliasRegisterResponseModel.getStatus().equalsIgnoreCase(DataTransState.CANCEL.name())){
			LOG.info("User hat abgebrochen");
			isCanceled(aliasRegisterResponseModel, principal);
			throw new CanceledAliasRegisterException();

		}
		else if(aliasRegisterResponseModel.getStatus().equalsIgnoreCase(DataTransState.ERROR.name())){
			LOG.error("Transactin von Datatrans ungültig");
			throw new DatatransException();
		}
		else {
			LOG.error("Transactin von Datatrans ungültig");
			throw new UnknownAliasregisterException();
		}

	}

	private void isCanceled(AliasRegisterResponeModel aliasRegisterResponseModel, Principal principal) {
		Optional<Transaction> opt = transactionRepository.findById(aliasRegisterResponseModel.getRefno());
		Transaction transaction;
		User user = userService.findUserByEmail(principal.getName());
		if (opt.isPresent()){
			transaction = opt.get();
			transaction.setState(TransactionState.ABGEBROCHEN);
			transaction.setUser(user);
			transactionRepository.save(transaction);
		}

	}


}
