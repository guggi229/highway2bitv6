/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.service;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import ch.highway2bit.datatrans.xsd.AuthorizationService;
import ch.highway2bit.datatrans.xsd.AuthorizationService.Body.Transaction;
import ch.highway2bit.datatrans.xsd.AuthorizationService.Body.Transaction.Request;
import ch.highway2bit.entity.User;
import ch.highway2bit.exception.NoAliasRegisterFoundException;
import ch.highway2bit.exception.PaymentException;
import ch.highway2bit.exception.UserInactivException;
import ch.highway2bit.mode.transaction.TransactionTyp;
import ch.highway2bit.model.global.Active;
import ch.highway2bit.model.global.Currency;
import ch.highway2bit.model.pay.PayConstans;

@Service
@Transactional
public class XmlService {


	private static final Logger LOG = LoggerFactory.getLogger(XmlService.class);
	private static final JAXBContext CONTEXT;

	static {
		try {
			CONTEXT = JAXBContext.newInstance(AuthorizationService.class);
		} catch (JAXBException e) {
			LOG.error("Fehler bei Setup von  JAXB Context", e);
			throw new ExceptionInInitializerError(e);
		}
	}

	@Autowired
	private UserService userService;

	@Autowired
	private TransactionService transactionService;

	@Value("${app.xsd.source}")
	private String xsd_source;

	@Value("${datatrans.merchantId}")
	private String merchantId_3d;

	@Value("${datatrans.sign.pay}")
	private String sign;




	/**
	 * Hier wird das Request XML für den Request zu Datatrans gebaut.
	 * @param email
	 * @param amount
	 * @return
	 * @throws JAXBException
	 * @throws RefNoAlreadyExistException
	 * @throws NoAliasRegisterFoundException
	 * @throws UserInactivException
	 */
	public String generateRequestXml(String email, Long amount) throws JAXBException, NoAliasRegisterFoundException, UserInactivException{
		AuthorizationService authorizationService = new AuthorizationService();
		User user = userService.findUserByEmail(email);

		// Prüfen ob User bereits einen Alias hat
		if (user.getAlias().getAliasCc()==null){
			// Ohne Alias kann keine Zahlung gemacht werden
			throw new NoAliasRegisterFoundException();
		}

		if (user.getActive().equals(Active.IN_ACTIVE)){
			// Wenn user inaktiv ist, dann darf keine Zahlung gemacht werden
			throw new UserInactivException();
		}

		Transaction transaction = new Transaction();
		transaction.setRefno(transactionService.createTransaction(TransactionTyp.PAY_WITH_ALIAS).getRefNo());

		// Bauen des XMLs für Datatrans
		Request request = new Request();
		request.setAmount(amount);
		request.setCurrency(Currency.CHF.name());
		request.setReqtype(PayConstans.REQUEST_TYP);
		request.setAliasCC(user.getAlias().getAliasCc());
		request.setExpm(user.getAlias().getExpiryMonth());
		request.setExpy(user.getAlias().getExpiryYear());
		request.setSign(sign);
		transaction.setRequest(request);

		AuthorizationService.Body body = new AuthorizationService.Body();
		body.setTransaction(transaction);
		body.setMerchantId(merchantId_3d);
		authorizationService.setBody(body);
		authorizationService.setVersion(PayConstans.VERSION);

		// Object to XML
		Marshaller marshaller = CONTEXT.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		final StringWriter swRequest = new StringWriter();
		marshaller.marshal(authorizationService, swRequest);
		String requestXML = swRequest.toString();
		LOG.info("Request XML: {}",requestXML);
		return requestXML;
	}

	/**
	 * Die Antwort von Datatrans (das XML response) wird hier in ein Java Objekt gemappt.
	 * @param inputXml
	 * @return
	 * @throws PaymentException
	 * @throws MalformedURLException
	 * @throws SAXException
	 */
	public AuthorizationService generateObjectFromXML(String inputXml) throws PaymentException {
		AuthorizationService authorizationService=null;
		try {
			Unmarshaller unm = CONTEXT.createUnmarshaller();

			// Übergibt dem Unmarshaller das Schema (XSD). Das XML das als Anwort kommt, wird somit gegen das xsd geprüft.
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			URL schemaUrl = AuthorizationService.class.getClassLoader().getResource(xsd_source).toURI().toURL();
			Schema schema = schemaFactory.newSchema(schemaUrl);
			unm.setSchema(schema);

			// Hier wird das eigentliche XML in das Java Object deserialisiert.
			authorizationService = (AuthorizationService) unm.unmarshal(new StringReader(inputXml));

		} catch (JAXBException | MalformedURLException | URISyntaxException | SAXException e) {
			LOG.error("Unbekannter Fehler: {}", e);
			throw new PaymentException();
		}
		return authorizationService;
	}

}
