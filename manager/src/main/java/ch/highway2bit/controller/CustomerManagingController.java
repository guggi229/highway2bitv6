/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ch.highway2bit.entity.NfcCard;
import ch.highway2bit.entity.User;
import ch.highway2bit.entity.subscription.SubscriptionDefinition;
import ch.highway2bit.exception.ElementAlreadyExistException;
import ch.highway2bit.exception.EmailAddressAlreadyExistException;
import ch.highway2bit.exception.UserNotFoundException;
import ch.highway2bit.service.SubscriptionDefinitionService;
import ch.highway2bit.service.UserService;

@Controller
@RequestMapping("/customermanager/")
public class CustomerManagingController extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(CustomerManagingController.class);

	// NAVIGATION
	public static final String NAV_TO_INDEX="customermanager/index";
	public static final String NAV_TO_ADD="customermanager/add";
	public static final String NAV_TO_EDIT="customermanager/edit";

	// EVENTS
	public static final String EVENT_INDEX="index";
	public static final String EVENT_ADD="add";
	public static final String EVENT_EDIT="edit/{id}";
	public static final String EVENT_DELETE="delete/{id}";
	public static final String EVENT_LOCK="lock/{id}";
	public static final String EVENT_READ_NFC_TAG="readnfc";

	@Autowired
	private UserService userService;

	@Autowired
	private SubscriptionDefinitionService subscirptionDefService;

	@ModelAttribute("customers")
	public Set<User> getUsers(){
		return userService.finaAllNotDeletedUser();
	}

	@ModelAttribute("subscirptions")
	public List<SubscriptionDefinition> getAllSubs(){
		return subscirptionDefService.findAll();
	}

	@GetMapping(EVENT_INDEX)
	public String index(Model model) throws IOException, Exception {
		model.addAttribute("customers", userService.finaAllNotDeletedUser());
		return NAV_TO_INDEX;
	}

	@GetMapping(EVENT_EDIT)
	public String showEditView(@PathVariable("id") long id, Model model) {
		Optional<User> opt = userService.findById(id);
		if (opt.isPresent()){
			model.addAttribute("user", opt.get());
		}
		else{
			messageError(model, "Da ist was schief gelaufen");
			return NAV_TO_INDEX;
		}
		return NAV_TO_EDIT;
	}

	@PostMapping(EVENT_EDIT)
	public String edit(@Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return NAV_TO_ADD;
		}
		userService.save(user);
		messageSucces(model, "Daten aktualisiert");
		model.addAttribute("customers", userService.finaAllNotDeletedUser());
		return NAV_TO_INDEX;
	}

	@GetMapping(EVENT_ADD)
	public String add(User user, Model model) {
		return NAV_TO_ADD;
	}

	@PostMapping(path =EVENT_ADD)
	public String addCustomer(@Valid User user, @ModelAttribute("sub") Long subId, BindingResult result, Model model) {

		if (result.hasErrors()) {
			return NAV_TO_ADD;
		}
		// Kein Wert für die NFC Karte eingegeben. Prop auf null setzten.
		if(user.getNfcCard().getId().isEmpty()){
			NfcCard card= null;
			user.setNfcCard(card);
		}

		try {
			userService.newCustomer(user, subId);
		} catch (ElementAlreadyExistException  e) {
			messageError(model, "Diese NFC Karte gibt es bereits im System!");
			LOG.error("NFC Karte bereits vergeben{}", e);
			return NAV_TO_ADD;
		} catch (EmailAddressAlreadyExistException e) {
			messageError(model, "Diese Email Adresse gibt es bereits im System!");
			LOG.error("Email Adresse bereits vergeben{}", e);
			return NAV_TO_ADD;
		}
		messageSucces(model,"Neuer Kunde wurde gespeichert");
		model.addAttribute("customers", userService.finaAllNotDeletedUser());
		return NAV_TO_INDEX;
	}

	@GetMapping(EVENT_DELETE)
	public String delete(@PathVariable("id") long id, Model model) {
		try {
			userService.delete(id);
			messageSucces(model,"Kunde gelöscht");
			LOG.info("User gelöscht");
		} catch (UserNotFoundException e) {
			messageError(model, "Löschung nicht möglich");
			LOG.error("User mit ID: {} nicht gefunden", id);
		}
		model.addAttribute("customers", userService.finaAllNotDeletedUser());
		return NAV_TO_INDEX;
	}

	@GetMapping(EVENT_LOCK)
	public String lock(@PathVariable("id") long id, Model model) {
		try {
			userService.lockOrUnlockUser(id);
			messageSucces(model,"Kunde gesperrt");
			LOG.info("User gelöscht");
		} catch (UserNotFoundException e) {
			messageError(model, "Sperrung nicht möglich");
			LOG.error("User mit ID: {} nicht gefunden", id);
		}
		model.addAttribute("customers", userService.finaAllNotDeletedUser());
		return NAV_TO_INDEX;
	}

	@GetMapping(EVENT_READ_NFC_TAG)
	public String readnfc(User user, BindingResult result, Model model) {
		return NAV_TO_ADD;
	}



}
