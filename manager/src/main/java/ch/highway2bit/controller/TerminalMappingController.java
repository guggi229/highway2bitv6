/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.highway2bit.entity.CookieMacMapper;
import ch.highway2bit.model.MappingTranferObject;
import ch.highway2bit.service.TerminalService;

/**
 * Dieser Controller dient zum Mappen des NFC reader mit der Theken Station.
 *
 * Bei Eingabe der ID wird ein Cookie zum Mappen Erstellt.
 * @author guggisberste
 *
 */

@Controller
@RequestMapping("/terminal/")
public class TerminalMappingController extends BaseController {

	private static final Logger LOG = LoggerFactory.getLogger(TerminalMappingController.class);

	private static int age = 60*60*24*365; // Setze Cookie ein Jahr

	@Autowired
	private TerminalService terminalService;

	// NAVIGATION
	private static final String MAP_ME = "terminal/add";

	// EVENTS
	private static final String EVENT_MAP_IT ="mapit";
	private static final String EVENT_SAVE ="save";

	// ATTRIBUT
	private static final String ATTRIBUT_ID= "id";




	@GetMapping(EVENT_MAP_IT)
	public String index(Model model) throws UnknownHostException, SocketException{
		model.addAttribute("mappingTranferObject", new MappingTranferObject());
		return MAP_ME;
	}

	@PostMapping(EVENT_SAVE)
	public String save(@Valid MappingTranferObject mappingTranferObject,BindingResult bindingResult,Model model,HttpServletResponse response){


		// Fall 1: Keine Wert erhalten oder keine Zahl
		// ===========================================
		if (bindingResult.hasErrors()) {
			messageError(model, bindingResult.getFieldError().getDefaultMessage());
			return MAP_ME;
		}

		Optional<CookieMacMapper> opt = terminalService.cookieMacMapperfindById(mappingTranferObject.getId());

		// Fall 2: ID gibt es nicht
		// ========================
		if (!opt.isPresent()){
			messageError(model, "Keine ID gefunden!");
			return MAP_ME;
		}

		CookieMacMapper cookieMacMapper = opt.get();

		// Fall 3: ID bereits verwendet
		// ============================
		if (!cookieMacMapper.getMac().isEmpty()){
			messageError(model, "ID und MAC bereits gemappt!");
		}

		// Fall 4: Alles ok. ID und MAC-Adresse mappen
		// ===========================================
		LOG.info("Erstelle neues Cookie! ID: {}", mappingTranferObject.getId());
		Cookie cookie = new Cookie(ATTRIBUT_ID, mappingTranferObject.getId().toString());
		cookie.setMaxAge(age);
		response.addCookie(cookie);
		LOG.info("Cookie erstellt!");
		messageSucces(model, "Gerät ist verbunden!");

		return MAP_ME;
	}


}
