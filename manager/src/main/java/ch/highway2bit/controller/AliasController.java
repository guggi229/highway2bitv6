/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import java.security.Principal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import ch.highway2bit.entity.PaymentMethod;
import ch.highway2bit.entity.User;
import ch.highway2bit.exception.CanceledAliasRegisterException;
import ch.highway2bit.exception.DatatransException;
import ch.highway2bit.exception.PaymentMethodNotSupportedException;
import ch.highway2bit.exception.UnknownAliasregisterException;
import ch.highway2bit.model.alias.AliasRegisterRequestModel;
import ch.highway2bit.model.alias.AliasRegisterResponeModel;
import ch.highway2bit.model.alias.UserDTO;
import ch.highway2bit.service.AliasService;
import ch.highway2bit.service.UserService;

@Controller
@RequestMapping("/alias/")
public class AliasController extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(AliasController.class);
	// NAVIGATION
	public static final String NAV_TO_ALIAS_REG="alias/aliasreg";
	public static final String NAV_TO_ALIAS_REG_REDIRECT="redirect:aliasregform";

	// EVENTS
	public static final String EVENT_ALIAS_REG="aliasreg";
	public static final String EVENT_ALIAS_REGISTRIERUNG="registrierung";
	public static final String EVENT_ALIAS_REGFORM="aliasregform";
	public static final String EVENT_ALIAS_DELETE="delete";

	@Autowired
	private AliasService aliasService;

	@Autowired
	private UserService userService;

	private AliasRegisterRequestModel aliasRegisterTransactionModel;

	/**
	 * Liefert die Zahlungsinformationen, falls vorhanden, dem Frontend.
	 * @param principal
	 * @return
	 */
	@ModelAttribute("paymentinfo")
	public PaymentMethod getPaymentInfo(Principal principal) {
		User user = userService.findUserByEmail(principal.getName());
		if(user.getAlias()!=null) {
			return user.getAlias().getPmethode();
		}
		return null;
	}

	@ModelAttribute("userinfo")
	public UserDTO getUserInfo(Principal principal) {
		User user = userService.findUserByEmail(principal.getName());
		UserDTO publicDTO= new UserDTO();
		publicDTO.setAlias(user.getAlias());
		return publicDTO;
	}

	@PostMapping(EVENT_ALIAS_DELETE)
	public String  del(Principal principal, Model model)  {

		aliasService.deleteAlias(principal);
		messageSucces(model, "Alias konnte gelöscht werden");


		return NAV_TO_ALIAS_REG_REDIRECT;
	}

	/**
	 * Wird von Datatrans aufgerufen und erhält die Antwort der AliasRegistrierung.
	 * @param allParams
	 * @param model
	 * @return
	 */
	@GetMapping(EVENT_ALIAS_REG)
	public String  aliasReg(AliasRegisterResponeModel aliasRegisterResponseModel, Model model, Principal principal) {
		LOG.info("Antwort von PSP erhalten");

		try {
			aliasService.saveAlias(aliasRegisterResponseModel, principal);
			messageSucces(model, "Sie haben sich erfolgreich registriert");
		} catch (UnknownAliasregisterException | DatatransException  e) {
			messageError(model, "Da ist was schief gelaufen");
			LOG.error("Fehler bei der AliasRegistrierung");
		} catch (CanceledAliasRegisterException e) {
			messageInfo(model, "Sie haben die Registrierung abgebrochen");
		} catch (PaymentMethodNotSupportedException e) {
			messageError(model, "Das Zahlungsmittel wird nicht unterstützt");
			LOG.error("Zahlungsmittel nicht unterstützt");
		}

		return NAV_TO_ALIAS_REG_REDIRECT;
	}

	@GetMapping(EVENT_ALIAS_REGFORM)
	public String  aliasRegForm() {
		return NAV_TO_ALIAS_REG;
	}

	/**
	 * Alias Registrierung erstellen.
	 * Redirect zu Datatrans
	 *
	 * @param attributes
	 * @return
	 */
	@PostMapping(EVENT_ALIAS_REGISTRIERUNG)
	public RedirectView redirect(RedirectAttributes attributes, Principal principal){

		aliasRegisterTransactionModel = aliasService.generateAliasModel(principal);

		attributes.addAttribute("merchantId", aliasRegisterTransactionModel.getMerchantId());
		attributes.addAttribute("amount", aliasRegisterTransactionModel.getAmount());
		attributes.addAttribute("currency", aliasRegisterTransactionModel.getCurrency());
		attributes.addAttribute("refno", aliasRegisterTransactionModel.getRefNo());
		attributes.addAttribute("sign", aliasRegisterTransactionModel.getSign());
		attributes.addAttribute("successUrl", aliasRegisterTransactionModel.getSuccessUrl());
		attributes.addAttribute("errorUrl", aliasRegisterTransactionModel.getErrorUrl());
		attributes.addAttribute("cancelUrl", aliasRegisterTransactionModel.getCancelUrl());
		attributes.addAttribute("uppWebResponseMethod",aliasRegisterTransactionModel.getUppWebResponseMethod());
		attributes.addAttribute("theme", aliasRegisterTransactionModel.getTheme());
		attributes.addAttribute("version", aliasRegisterTransactionModel.getVersion());
		attributes.addAttribute("uppReturnTarget", aliasRegisterTransactionModel.getUppReturnTarget());
		attributes.addAttribute("useAlias", aliasRegisterTransactionModel.getUseAlias());

		return new RedirectView(aliasService.getRequestUrl());
	}


}
