/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import ch.highway2bit.exception.PasswordDoesNotMatchException;
import ch.highway2bit.exception.TokenExpireException;
import ch.highway2bit.exception.TokenNotFoundException;
import ch.highway2bit.model.user.register.GeneratePasswordTransactionModel;
import ch.highway2bit.service.UserService;

@Controller
@RequestMapping("/register/")
public class RegistrationController extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(RegistrationController.class);

	// NAVIGATION
	public static final String NAV_TO_CONFIRM_REGISTER="/register/regitrationConfirm";
	public static final String NAV_TO_ROOT="/login";

	// EVENTS
	public static final String EVENT_REGISTER_CONFIRM="regitrationConfirm";
	public static final String EVENT_REGISTER_ACTIVATE="activate";

	@Autowired
	private UserService userService;

	@GetMapping(value=EVENT_REGISTER_CONFIRM)
	public String confirmRegistration (WebRequest request, Model model, @RequestParam("token") String token) {
		model.addAttribute("pass", new GeneratePasswordTransactionModel());
		model.addAttribute("token" , token);
		try {
			userService.checkToken(token);
		} catch (TokenNotFoundException e) {
			messageError(model, "Unbekannter Fehler");
			LOG.error("Token nicht gefunden");
		} catch (TokenExpireException e) {
			messageError(model, "Token abgelaufen");
			LOG.info("Token abgelaufen");
		}

		return NAV_TO_CONFIRM_REGISTER;
	}

	@PostMapping(value=EVENT_REGISTER_ACTIVATE)
	public String activateUser(Model model, @Valid GeneratePasswordTransactionModel pass, BindingResult result,@ModelAttribute("token") String token) {
		try {
			userService.activateUser(pass.getNewPassword(), pass.getRepeatedNewPassword(), token);
			messageInfo(model, "Du hast dich erfolgreich registriert! Danke!");
		} catch (TokenNotFoundException | TokenExpireException | PasswordDoesNotMatchException e) {
			messageError(model, "Fehler");
		}

		return NAV_TO_ROOT;
	}

}
