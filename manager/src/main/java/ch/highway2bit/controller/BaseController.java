/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import org.springframework.ui.Model;


/**
 * Hängt am Thymeleaf Model die Nachricht für den Benutzer an.
 *
 * @author guggisberste
 *
 */
public abstract class BaseController {

	/**
	 * Textanzeige, bei Erfolg. --> Grüne Anzeige
	 * @param model
	 * @param successText
	 * @return
	 */
	public Model messageSucces(Model model, String successText){
		model.addAttribute("message",successText);
		model.addAttribute("alertClass","alert-success");
		return model;
	}

	/**
	 * Textanzeige, bei Error. --> Rote Anzeige
	 * @param model
	 * @param errorText
	 * @return
	 */
	public Model messageError(Model model, String errorText){
		model.addAttribute("message",errorText);
		model.addAttribute("alertClass","alert-danger");
		return model;
	}

	/**
	 * Textanzeige, bei Info. --> Blauer Anzeige
	 * @param model
	 * @param infoText
	 * @return
	 */
	public Model messageInfo(Model model, String infoText){
		model.addAttribute("message",infoText);
		model.addAttribute("alertClass","alert-primary");
		return model;
	}
}
