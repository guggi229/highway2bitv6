/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.highway2bit.service.TransactionService;
import ch.highway2bit.service.UserService;

@Controller
@RequestMapping("/customer/")
public class CustomerController extends BaseController{
	// NAVIGATION
	public static final String NAV_TO_INDEX="customer/index";
	public static final String NAV_TO_TRANSACTIONLIST="customer/transactions";

	// EVENTS
	public static final String EVENT_INDEX="index";
	public static final String EVENT_TRANSACTIONS="transactions";


	@Autowired
	private UserService userService;

	@Autowired
	private TransactionService transactionService;

	@GetMapping(EVENT_INDEX)
	public String index() {
		return NAV_TO_INDEX;
	}

	@GetMapping(EVENT_TRANSACTIONS)
	public String transactions(Model model, Principal principal) {
		model.addAttribute("transactions", transactionService.findTransactionByCustomerId(userService.findUserByEmail(principal.getName()).getId()));
		return NAV_TO_TRANSACTIONLIST;
	}



}
