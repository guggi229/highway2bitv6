/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/groupmanager/")
public class GroupfitnessManagingController {

	// NAVIGATION
	public static final String NAV_TO_INDEX="groupmanager/index";

	// EVENTS
	public static final String EVENT_INDEX="index";

	@GetMapping(EVENT_INDEX)
	public String index(){
		return NAV_TO_INDEX;

	}

}
