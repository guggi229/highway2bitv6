/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController extends BaseController{
	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);
	// NAVIGATION
	public static final String NAV_LOGIN="login";

	// EVENTS
	public static final String EVENT_LOGIN="/login";
	public static final String EVENT_ROOT="/";

    @GetMapping(value={EVENT_LOGIN, EVENT_ROOT})
    public String login(Model model,@RequestParam(value = "error", required = false) Boolean error){

    	if (error!=null && error) {
    		LOG.info("Login fehlgeschlagen");
    		messageError(model, "Einloggen fehlgeschlagen");
    	}
    	  return NAV_LOGIN;
    }

}
