/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.highway2bit.entity.CookieMacMapper;
import ch.highway2bit.model.MessageContainer;
import ch.highway2bit.service.TerminalService;

@RestController
@RequestMapping("/terminal")
public class NfcReaderController {

	@Autowired
	private TerminalService terminalService;

	@GetMapping()
	public MessageContainer read(@CookieValue("id") String id) {
		MessageContainer messageContainer = new MessageContainer(null);
		Long longId =Long.parseLong(id);
		Optional<CookieMacMapper> opt = terminalService.cookieMacMapperfindById(longId);
		if (opt.isPresent()){
			messageContainer = new MessageContainer(opt.get().getNfcId());
		}
		return messageContainer;
	}

}
