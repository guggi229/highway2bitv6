/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import java.time.LocalDate;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ch.highway2bit.entity.Kurs;
import ch.highway2bit.entity.Kursrun;
import ch.highway2bit.entity.Room;
import ch.highway2bit.exception.TableLockedException;
import ch.highway2bit.exception.TimeOverlapException;
import ch.highway2bit.service.KursRunService;
import ch.highway2bit.service.KursService;
import ch.highway2bit.service.RoomService;

@Controller
@RequestMapping("/classrun/")
public class KursrunController extends BaseController{

	private static final Logger LOG = LoggerFactory.getLogger(KursrunController.class);

	// NAVIGATION
	public static final String NAV_TO_LIST="kursrun/list";
	public static final String NAV_TO_ADD="kursrun/add";


	// EVENTS
	public static final String EVENT_LIST="list";
	public static final String EVENT_ADD="add";

	@Autowired
	private KursRunService kursrunService;

	@Autowired
	private KursService kursService;

	@Autowired
	private RoomService roomService;


	@GetMapping(EVENT_LIST)
	public String showUpdateForm(Model model) {
		model.addAttribute("kursruns", kursrunService.findAllKursesNotDeleted());
		return NAV_TO_LIST;
	}

	@GetMapping(EVENT_ADD)
	public String add( Kursrun kursrun, Kurs kurs, Room room, Model model) {

		LocalDate localDate = LocalDate.now();
		model.addAttribute("kurse", kursService.findAllKursesNotDeleted());
		model.addAttribute("rooms", roomService.findAllRoomsNotDeleted());
		model.addAttribute("date",  localDate);
		return NAV_TO_ADD;
	}

	@PostMapping(EVENT_ADD)
	public String addClassrun(@Valid Kursrun kursrun, BindingResult result, Model model) {
		// Bei Validierungsfehlern
		if (result.hasErrors()) {
			messageError(model, "Bitte überprüfe die Eingabe");
			LocalDate localDate = LocalDate.now();
			model.addAttribute("kurse", kursService.findAllKursesNotDeleted());
			model.addAttribute("rooms", roomService.findAllRoomsNotDeleted());
			model.addAttribute("date",  localDate);
			return NAV_TO_ADD;
		}
		// Speichern des Kurses.
		try {
			kursrunService.create(kursrun);
		} catch (TimeOverlapException e) {
			// Falls sich der Kurs mit einem anderen überlappt:
			messageError(model, "Dieser Kurs überlappt sich mit einem anderen!");
			LocalDate localDate = LocalDate.now();
			model.addAttribute("kurse", kursService.findAllKursesNotDeleted());
			model.addAttribute("rooms", roomService.findAllRoomsNotDeleted());
			model.addAttribute("date",  localDate);
			return NAV_TO_ADD;
		} catch (TableLockedException e) {
			messageError(model, "Datensatz geperrt.");
			LocalDate localDate = LocalDate.now();
			model.addAttribute("kurse", kursService.findAllKursesNotDeleted());
			model.addAttribute("rooms", roomService.findAllRoomsNotDeleted());
			model.addAttribute("date",  localDate);
			return NAV_TO_ADD;
		}
		messageSucces(model, "Kursdurchführung gespeichert");
		LOG.info("Kursdurchführung erfolgreich gespeichert");
		return "redirect:list";
	}

}
