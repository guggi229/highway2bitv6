/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import java.security.Principal;
import java.text.MessageFormat;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.highway2bit.exception.PasswordDoesNotMatchException;
import ch.highway2bit.exception.WrongPasswordException;
import ch.highway2bit.model.user.register.ChangePasswordTransactionModel;
import ch.highway2bit.service.UserService;
import ch.highway2bit.utils.RegexConstants;

@Controller
@RequestMapping("/global/")
public class PasswortChangeController extends BaseController {

	private static final Logger LOG = LoggerFactory.getLogger(PasswortChangeController.class);

	public static final String NAV_TO_CHANGE_PASSWORD="global/changepassword";
	public static final String EVENT_PASSWORD_CHANGE = "changepass";

	@Autowired
	private UserService userService;

	@GetMapping(EVENT_PASSWORD_CHANGE)
	public String passwordchange(Model model) {
		model.addAttribute("pass", new ChangePasswordTransactionModel()); 
		return NAV_TO_CHANGE_PASSWORD;
	}

	@PostMapping(EVENT_PASSWORD_CHANGE)
	public String change(@Valid ChangePasswordTransactionModel pass, BindingResult result, Principal principal, Model model)  {
		 model.addAttribute("pass", new ChangePasswordTransactionModel());
		 if (result.hasErrors()) {
			 messageError(model, MessageFormat.format("Folgende Zeichen sind erlaubt: {0}", RegexConstants.ERLAUBTE_ZEICHEN_PASSWORT_PLAIN));
	            return NAV_TO_CHANGE_PASSWORD;
	        }
		try {
			userService.changePasswort(principal, pass.getPassword(), pass.getNewPassword(), pass.getRepeatedNewPassword());
			messageSucces(model, "Passwort geändert");
		} catch (WrongPasswordException e) {
			messageError(model, "Aktuelles Passwort falsch");
			LOG.error("Aktuelles Passwort stimmt nicht!");
		} catch (PasswordDoesNotMatchException e) {
			messageError(model, "Passwörter stimmen nicht überein!");
			LOG.error("Wiederholtes Passwort falsch");
		}
		return NAV_TO_CHANGE_PASSWORD;
	}
}
