/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ch.highway2bit.entity.Kurs;
import ch.highway2bit.exception.ElementAlreadyExistException;
import ch.highway2bit.service.KursService;

@Controller
@RequestMapping("/class/")
public class KursController extends BaseController {

	private static final Logger LOG = LoggerFactory.getLogger(KursController.class);

	// NAVIGATION
	public static final String NAV_TO_CREATE_CLASS_FORM="kurs/add";
	public static final String NAV_TO_LIST="kurs/list";
	public static final String NAV_TO_ADD="kurs/add";
	public static final String NAV_TO_EDIT="kurs/edit";

	// EVENTS
	public static final String EVENT_SHOW_CREATE_FORM="createClassForm";
	public static final String EVENT_LIST="list";
	public static final String EVENT_ADD="add";
	public static final String EVENT_DELETE="delete/{id}";
	public static final String EVENT_EDIT="edit/{id}";

	@Autowired
	private KursService kursService;

	@ModelAttribute("kurse")
	public Set<Kurs> getAllClasses(){
		return kursService.findAllKursesNotDeleted();
	}

	@GetMapping(EVENT_SHOW_CREATE_FORM)
	public String createClass() {
		return NAV_TO_CREATE_CLASS_FORM;
	}

	@GetMapping(EVENT_LIST)
	public String showUpdateForm(Model model) {
		model.addAttribute("kurse", kursService.findAllKursesNotDeleted());
		return NAV_TO_LIST;
	}

	@GetMapping(EVENT_ADD)
	public String showAddForm(Kurs kurs) {
		return NAV_TO_ADD;
	}

	@GetMapping(EVENT_DELETE)
	public String delete(@PathVariable("id") long id, Model model) {
		Optional<Kurs> opt = kursService.findById(id);
		if (opt.isPresent()){
			Kurs kurs = opt.get();
			kursService.delete(kurs);
			messageSucces(model, "Kurs gelöscht");
		}
		else messageInfo(model, "Kurs nicht gefunden");
		model.addAttribute("kurse", kursService.findAllKursesNotDeleted());
		return NAV_TO_LIST;
	}

	@PostMapping(EVENT_ADD)
	public String addKurs(@Valid Kurs kurs,BindingResult result , @RequestParam("file") MultipartFile file, Model model )  {
		if (result.hasErrors()) {
			return NAV_TO_ADD;
		}
		try {
			if(file.isEmpty()){
				LOG.info("Kein Bild erhalten");
			}
			else{
				kurs.setImage(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
			}
			kursService.create(kurs);
			messageSucces(model, "Kurs gespeichert");
		} catch ( IOException e) {
			LOG.error("Fehler beim Speichern des Kurses");
			messageError(model, "Da ist was schief gelaufen");
			return NAV_TO_ADD;
		} catch (ElementAlreadyExistException e) {
			LOG.error("Einen Kurs mit diesem Namen gibt es bereits");
			messageError(model, "Einen Kurs mit diesem Namen gibt es bereits");
			return NAV_TO_ADD;
		}
		return "redirect:list";
	}

	@GetMapping(EVENT_EDIT)
	public String showUpdateForm(@PathVariable("id") long id, Model model ) {
		Optional<Kurs> opt = kursService.findById(id);
		if (opt.isPresent()){
			Kurs kurs = opt.get();
			model.addAttribute("kurs", kurs);
		}
		return NAV_TO_EDIT;
	}

	@PostMapping("update/{id}")
	public String updateKurs(@Valid Kurs updatedKurs, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			return NAV_TO_EDIT;
		}

		Optional<Kurs> opt = kursService.findById(updatedKurs.getId());
		if (opt.isPresent()){
			Kurs kurs = opt.get();
			kurs.setKursName(updatedKurs.getKursName());
			try {
				kursService.save(kurs);
			} catch (ElementAlreadyExistException e) {
				LOG.error("Einen Kurs mit diesem Namen gibt es bereits");
				messageError(model, "Einen Kurs mit diesem Namen gibt es bereits");
			}
		}
		model.addAttribute("kurse", kursService.findAllKursesNotDeleted());
		return NAV_TO_LIST;
	}

}
