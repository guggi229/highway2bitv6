/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.highway2bit.entity.Kursrun;
import ch.highway2bit.service.KursRunService;
import ch.highway2bit.service.ParticipatedClassService;

@Controller
@RequestMapping("/dashboard/")
public class DashboardController {

	private static final Logger LOG = LoggerFactory.getLogger(DashboardController.class);
	private long slidingWindow = 10;


	@Autowired
	private ParticipatedClassService participatedClassService;

	@Autowired

	private KursRunService kursRunService;

	// NAVIGATION
	public static final String NAV_TO_TRAINER_DASHBOARD="/dashboard/showboard";

	// EVENTS
	public static final String EVENT_LIST_CUSTOMER="room/{roomId}";


	@GetMapping(EVENT_LIST_CUSTOMER)
	public String  showboard(@PathVariable(required = true) Long roomId,Model model) {
		Optional<Kursrun> opt = kursRunService.findById(roomId);
		if (opt.isPresent()) {
			Kursrun kursrun = opt.get();
			LOG.info("Kursdurchführung gefunden. Raum: {}",roomId);


			model.addAttribute("kunde",  participatedClassService.findUserByKursrun(roomId,LocalDate.now(),LocalTime.now().minusMinutes(slidingWindow), LocalTime.now().plusMinutes(slidingWindow)));
			model.addAttribute("room",  kursrun.getRoom());
		}
		else
		{
			LOG.info("Keine Kunden gefunden");
		}
		return NAV_TO_TRAINER_DASHBOARD;
	}
}
