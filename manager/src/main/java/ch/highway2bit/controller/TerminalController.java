/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/terminal/")
public class TerminalController {

	// NAVIGATION
	public static final String NAV_TO_TERMINAL ="/terminal/index";
	
	// EVENTS
	public static final String EVENT_SHOW_NEXT_CLASS="nextClass";
	
    @GetMapping(EVENT_SHOW_NEXT_CLASS)
    public String index(){
        return NAV_TO_TERMINAL;
    }
}
