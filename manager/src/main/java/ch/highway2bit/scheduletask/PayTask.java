/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.scheduletask;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ch.highway2bit.entity.ParticipatedClass;
import ch.highway2bit.entity.subscription.Subscription;
import ch.highway2bit.exception.PaymentException;
import ch.highway2bit.model.alias.ChargedState;
import ch.highway2bit.repository.ParticipatedClassRepository;
import ch.highway2bit.service.KursRunService;
import ch.highway2bit.service.PayService;
import ch.highway2bit.service.UserService;

/**
 * Hier wird die Tabelle ParticipatedClass durchsucht. Alle Kunden die in einen Kurs gehen, werden in dieser Tabelle erfasst.
 * Diese wird alle x [ms] durchsucht. Alle Besuche mit dem Status 0 wurden noch nicht verrechnet und es wird versucht, sie abzurechnen bzw.
 * Das ABo des Kunden wird geladen und mit der Methode Validiert.
 * Dabei wird die PayService Referenz mitgegegben, da wir nicht mit im Spring-Boot Context sind.
 * 
 * 
 * @author guggi229
 *
 */
@Component
public class PayTask {

	private static final Logger LOG = LoggerFactory.getLogger(PayTask.class);
	@Autowired
	private PayService payService;

	@Autowired
	private ParticipatedClassRepository participatedClassRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private KursRunService kursrunService;

	/**
	 * prüft ob es neues Jobs gibt. Die Tabelle ParticipatedClass wird dazu durchsucht und alle Elemente mit charged=open geladen.
	 * Elemente mit Error werden ignoriert. Diese müssen von Hand kontrolliert werden.
	 *
	 */
	@Scheduled(fixedRate = 20000)
	public void checkPayQueue(){
		LOG.info("Prüfe auf neue Pay-Jobs");
		// Lade alle Kursbesuche mit Status Open (Somit nicht bezahlte Kursbesuche)
		Collection<ParticipatedClass> participatedClasses = participatedClassRepository.findByCharged(ChargedState.OPEN);
		if (!participatedClasses.isEmpty()){
			for (ParticipatedClass participatedClass : participatedClasses) {
				try {
					Subscription subscirption = participatedClass.getUser().getSubscirption();
					// Übergebe die Instanzen dem Pay Objekt.
					subscirption.validateSubscription(payService, userService, kursrunService, participatedClass);
				} catch (PaymentException e) {
					LOG.error("Fehler im Job: ", e);
				}

			}
		}

	}

}
