/*************************************************************************************
 * Copyright Stefan Guggisberg
 * 
 * Bachelorarbeit 2019-2020
 * 
 * Projekt: Highway2Bit
 * 
 * 
*************************************************************************************/

package ch.highway2bit.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;


/**
 * Wird als Tranfer Objekt für die Cookie ID verwendet.
 * @author guggi229
 *
 */

public class MappingTranferObject implements Serializable {
	private static final long serialVersionUID = -5984558859494819554L;

	@NotNull(message = "Achtung! Feld draf nicht leer sein und muss eine Zahl sein!")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
