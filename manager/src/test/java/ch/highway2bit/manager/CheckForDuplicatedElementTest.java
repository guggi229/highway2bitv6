package ch.highway2bit.manager;


import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import ch.highway2bit.entity.Kurs;
import ch.highway2bit.exception.ElementAlreadyExistException;
import ch.highway2bit.repository.KursRepository;
import ch.highway2bit.service.KursService;

import static org.junit.Assert.fail;
import org.mockito.Mockito;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;


/**
 *
 * @author guggisberste
 *
 */
@RunWith(MockitoJUnitRunner.class)

public class CheckForDuplicatedElementTest {

	@Mock
	KursRepository kursRunRepoMock;

	@InjectMocks
	KursService kursService;

	/**
	 * Positiver Test
	 *
	 * Neuer Kurs erfassen mit anderem Kursname!
	 *
	 */

	@Test
	public void checkElemetAlreadyExistTest() {

		// Referenz "Simulieren Eintrag in DB"
		Kurs kurs1 = new Kurs();
		kurs1.setId(1L);
		kurs1.setIsDelete(false);
		kurs1.setKursName("Kursname");

		Optional<Kurs> opt = Optional.of(kurs1);

		Mockito.lenient().when(kursRunRepoMock.findByKursName("Kursname")).thenReturn(opt);

		Kurs kurs2 = new Kurs();
		kurs2.setId(2L);
		kurs2.setIsDelete(Boolean.FALSE);
		kurs2.setKursName("Kursname2");
		try {
			kursService.create(kurs2);
		} catch (ElementAlreadyExistException e) {
			// Darf nicht passieren!
			fail("Fehler!");
		}

	}

	/**
	 * Negativer  Test
	 *
	 * Neuer Kurs erfassen mit anderem Kursname!
	 * @throws ElementAlreadyExistException
	 *
	 */

	//@Test(expected = ElementAlreadyExistException.class)
	public void checkElemetAlreadyExistTest2() throws ElementAlreadyExistException  {

		// Referenz "Simulieren Eintrag in DB"
		Kurs kurs1 = new Kurs();
		kurs1.setId(1L);
		kurs1.setKursName("Kursname");

		Optional<Kurs> opt = Optional.of(kurs1);

		Mockito.lenient().when(kursRunRepoMock.findByKursName("Kursname")).thenReturn(opt);

		Kurs kurs2 = new Kurs();
		kurs2.setId(2L);
		kurs2.setKursName("Kursname");
			kursService.create(kurs2);
	}

}
