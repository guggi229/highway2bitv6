var canvas = null;
var base64Image= null;

var stompFailureCallback = function (error) {
    console.log('STOMP: ' + error);
    setTimeout(connect, 5000);
    console.log('STOMP: Reconecting in 5 seconds');
};

$('document').ready(connect());
function connect() {

	var customerinfosocket = new SockJS('/websocket');
	stompClient = Stomp.over(customerinfosocket);
	stompClient.connect({}, function (frame) {
		console.log('Connected: ' + frame);
		init()
		stompClient.subscribe('/topic/customerinfo', function (data) {
			customerinfo(JSON.parse(data.body).content);
		});
		stompClient.subscribe('/topic/nextclass', function (data) {
			showNextClass(JSON.parse(data.body).content);
		});
	}, stompFailureCallback);
}

function init() {
	console.log("Init Data");
	stompClient.send("/app/init",{},"");
}

function customerinfo(message) {
	var obj = JSON.parse(message);
	console.log("Message:" + message);
	console.log(obj.displayinfo);
	console.log(obj.hasError);
	if (obj.hasError){
		toastr.error(obj.displayinfo);

		document.getElementById("tools_sketch").classList.remove('image');
		document.getElementById("tools_sketch").classList.remove('success');
		document.getElementById("tools_sketch").classList.add("error");
	}
	else{

		document.getElementById("tools_sketch").classList.remove('error');
		document.getElementById("tools_sketch").classList.remove('image');
		document.getElementById("tools_sketch").classList.add("success");

	}

	remoceclass();
}


function showNextClass(message) {
	console.log("Meldung: " + message);
	// Vorbereitung JSON --> JavaScript Objekt
	var obj = JSON.parse(message);
	console.log('Kurs:' + obj.kursName);
	console.log('Raum:' + obj.raumName);

	// Raumname schreiben
	var raumname = document.createTextNode(obj.raumName);
	var itemraumname = document.getElementById("raumname");
	itemraumname.innerText=(raumname.textContent);

	if (obj.imageBase64!=null){
		if (document.getElementById("image") !=null){
			document.getElementById("image").style.visibility = "hidden";
		}
		// Bild laden
		console.log("Lade Bild");
		base64Image = document.createTextNode(obj.imageBase64.data);
		canvas = document.getElementById("tools_sketch");
		var	ctx = canvas.getContext("2d");

		image = new Image();
		image.src = "data:image/png;base64,"+obj.imageBase64.data;
		image.onload = function() {
			ctx.drawImage(image, 0, 0);
		};
		document.getElementById("tools_sketch").style.visibility = "visible";
	}
	else{
		document.getElementById("tools_sketch").style.visibility = "hidden";
	}

	// Kursname schreiben
	var kursname = document.createTextNode(obj.kursName);
	var itemkursname = document.getElementById("kursname");
	itemkursname.innerText=(kursname.textContent);
	


}

function remoceclass() {
    setTimeout(function () {
    	document.getElementById("tools_sketch").classList.remove('success');
    	document.getElementById("tools_sketch").classList.remove('error');
    	document.getElementById("tools_sketch").classList.add("image");
    	toastr.clear()

    }, 1000);
    return false;
};


