package ch.highway2bit.controller;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Base64;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.highway2bit.entity.Kursrun;
import ch.highway2bit.model.NextClassRunDisplayInfo;
import ch.highway2bit.model.CustomerDisplayInfo;
import ch.highway2bit.model.MessageContainer;
import ch.highway2bit.model.global.TerminalId;
import ch.highway2bit.service.CheckpointTerminal;
import ch.highway2bit.service.KursRunService;
import ch.highway2bit.service.TerminalKursrunService;

@Controller
@RequestMapping("/terminal/")
public class TerminalController {
	private static final Logger LOG = LoggerFactory.getLogger(TerminalController.class);

	// NAVIGATION
	public static final String NAV_TO_NEXT_CLASS_VIEW = "terminal/nextclass";

	// EVENTS
	public static final String EVENT_NEXT_CLASS ="nextclass";

	// Attribut
	public static final String ATTRIBUT_NEXT_CLASS = "displayinfo";
	public static final String ATTRIBUT_ROOM_NAME = "roomname";
	public static final String ATTRIBUT_IMAGE = "image";

	@Autowired
	private SimpMessagingTemplate smt;

	@Autowired
	private TerminalKursrunService terminalKursrunService;

	@Autowired
	private KursRunService kursRunService;

	@Autowired
	private TerminalId terminalId;

	private ObjectMapper objectMapper;

	/**
	 * Laden der nächsten Kursdurchführung
	 * @param model
	 * @return
	 * @throws UnknownHostException
	 * @throws SocketException
	 */
	@GetMapping(EVENT_NEXT_CLASS)
	public String index() throws UnknownHostException, SocketException{
		return NAV_TO_NEXT_CLASS_VIEW;
	}

	/**
	 * Sendet die neuen Kursdaten zum Frontend (Socket)
	 *
	 * @return
	 */

	public void sendNextClassRunInfo(Kursrun kursrun) {
		// Tranfer Objekt für das Frontend erzeugen
		NextClassRunDisplayInfo displayInformationen = new NextClassRunDisplayInfo();
		String displayInformationenAsString="";
		objectMapper = new ObjectMapper();

		// Infos zusammen suchen
		displayInformationen.setKursName(kursrun.getKurs().getKursName());
		displayInformationen.setRaumName(terminalId.getRoom().getName());
		displayInformationen.setImage(kursrun.getKurs().getImage());
		try {
			displayInformationenAsString = objectMapper.writeValueAsString(displayInformationen);
		} catch (JsonProcessingException e) {
			LOG.error("Fehler beim Erstellen des JSON Objekts");
		}

		LOG.info("Informiere alle Observer");
		smt.convertAndSend("/topic/nextclass", new MessageContainer(displayInformationenAsString));
	}

	@MessageMapping("/init")
	public void init(){
		Optional<Kursrun> opt = terminalKursrunService.findNextKursrun();
		if (opt.isPresent()){
			sendNextClassRunInfo(opt.get());
		}
		else{
		sendNextClassRunInfo(kursRunService.getDummyClass());
		}
	}

	public void sendCustomerInfo (String info, boolean state){
		CustomerDisplayInfo customerDisplayInfo = new CustomerDisplayInfo();
		customerDisplayInfo.setDisplayInfo(info);
		customerDisplayInfo.setHasError(state);
		objectMapper = new ObjectMapper();
		String displayInformationenAsString="";
		try {
			displayInformationenAsString = objectMapper.writeValueAsString(customerDisplayInfo);
		} catch (JsonProcessingException e) {
			LOG.error("Fehler beim Erstellen des JSON Objekts");
		}
		smt.convertAndSend("/topic/customerinfo", new MessageContainer(displayInformationenAsString));
	}

	public void sendCustomerInfo (boolean state){
		sendCustomerInfo("", state);
	}


}
