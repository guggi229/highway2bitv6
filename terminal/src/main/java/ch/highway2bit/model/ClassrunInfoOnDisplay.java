package ch.highway2bit.model;

import java.io.Serializable;

/**
 * Hält die aktuelle Kursnummer fest.
 * Wird im Applicationscope instanziert.
 *
 * @author guggisberste
 *
 */
public class ClassrunInfoOnDisplay implements Serializable {

	private static final long serialVersionUID = 2918481499753184572L;
	private Long id;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


}
