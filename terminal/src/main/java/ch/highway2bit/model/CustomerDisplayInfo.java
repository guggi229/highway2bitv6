package ch.highway2bit.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerDisplayInfo implements Serializable {
	private static final long serialVersionUID = 271714996901815224L;

	@JsonProperty("displayinfo")
	private String displayInfo;

	@JsonProperty("hasError")
	private boolean hasError;

	public String getDisplayInfo() {
		return displayInfo;
	}

	public void setDisplayInfo(String displayInfo) {
		this.displayInfo = displayInfo;
	}

	public boolean isHasError() {
		return hasError;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}

}
