package ch.highway2bit.model;

import java.io.Serializable;
import org.bson.types.Binary;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Dient als Tranfer-Objekt von backend zu Frontend
 * @author guggisberste
 *
 */
public class NextClassRunDisplayInfo implements Serializable{

	private static final long serialVersionUID = -3570414592188707925L;

	@JsonProperty("kursName")
	private String kursName;

	@JsonProperty("raumName")
	private String raumName;

	@JsonProperty("imageBase64")
	private Binary imageBase64;



	public String getKursName() {
		return kursName;
	}

	public void setKursName(String kursName) {
		this.kursName = kursName;
	}

	public String getRaumName() {
		return raumName;
	}

	public void setRaumName(String raumName) {
		this.raumName = raumName;
	}

	public Binary getImage() {
		return imageBase64;
	}

	public void setImage(Binary imageBase64) {
		this.imageBase64 = imageBase64;
	}

	public byte[] getImageData(){
		return this.imageBase64.getData();
	}

}
