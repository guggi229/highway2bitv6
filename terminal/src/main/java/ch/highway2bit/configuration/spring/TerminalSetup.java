package ch.highway2bit.configuration.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import ch.highway2bit.model.ClassrunInfoOnDisplay;
import ch.highway2bit.model.global.TerminalId;


@Configuration
public class TerminalSetup {

	/**
	 * Hier setzten wir das Objekt TerminalId in den Applikationsscope. Somit besteht dieses Objekt einmal über die ganze Applikation.
	 * Dabei werden Informationen über das Terminal gespeichert:
	 *
	 * Parameter:
	 * Mac Adresse
	 * Raum Id
	 */
	@Bean
	@Scope(value = WebApplicationContext.SCOPE_APPLICATION)
	public TerminalId getTerminalIdModel() {
		return new TerminalId();
	}

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_APPLICATION)
	public ClassrunInfoOnDisplay getClassrunInfoOnDisplay() {
		return new ClassrunInfoOnDisplay();
	}

}
