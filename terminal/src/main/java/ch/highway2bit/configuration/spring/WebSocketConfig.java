package ch.highway2bit.configuration.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {

		/**
		 * Definiert den Endpoint für die Nachrichten zum Client
		 */
		config.enableSimpleBroker("/topic");

		/**
		 * Definiert den Endpoint die der Client benutzt um Nachrichten zu senden
		 */
		config.setApplicationDestinationPrefixes("/app");
	}

	/**
	 * Als Protokoll verwenden wir STOMP und fügt ein extra Layer ein.
	 *
	 */
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/websocket").withSockJS();
	}

	@Override public void configureWebSocketTransport(WebSocketTransportRegistration registration) {
		registration.setSendTimeLimit(60 * 1000).setSendBufferSizeLimit(200 * 1024 * 1024).setMessageSizeLimit(200 * 1024 * 1024);
	}

}