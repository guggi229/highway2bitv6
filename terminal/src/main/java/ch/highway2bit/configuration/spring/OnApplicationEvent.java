package ch.highway2bit.configuration.spring;

import java.net.SocketException;
import java.util.Optional;

import javax.smartcardio.CardException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import ch.highway2bit.entity.Terminal;
import ch.highway2bit.model.global.TerminalId;
import ch.highway2bit.service.CheckpointTerminal;
import ch.highway2bit.service.TerminalService;
import ch.highway2bit.utils.MacReader;


@SuppressWarnings("restriction")
@Component
public class OnApplicationEvent {

	private static final Logger LOG = LoggerFactory.getLogger(OnApplicationEvent.class);

	@Autowired
	private TerminalId terminalId;

	@Autowired
	private TerminalService terminalService;

	@Autowired
	private CheckpointTerminal checkpointTerminal;

	@EventListener
	public void onApplicationEvent(ContextRefreshedEvent event) throws InterruptedException, CardException {
		LOG.info("Setup Terminal:");
		MacReader macreader = new MacReader();

		Optional<Terminal> ter;
		try {
			ter = terminalService.findById(macreader.getMacAddess());

			if (ter.isPresent()){
				terminalId.setMac(ter.get().getMac());
				terminalId.setRoom(ter.get().getRoom());
				LOG.info("************************************");
				LOG.info("Starte der Applikation");
				LOG.info("Mac: " + ter.get().getMac());
				LOG.info("Raum: " + ter.get().getRoom().getName());
				LOG.info("************************************");
			}
			else {
				LOG.info("Kein Eintrag in der DB über die Terminal Zugehörigkeit!");
				checkpointTerminal.readReceiver(terminalService);
				
			}
		} catch (SocketException e) {
			LOG.error("Fehler beim Lesen der MacAdresse. System wird beendet! {}", e);
			System.exit(0);
		}

	}
}