package ch.highway2bit.scheduletask;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ch.highway2bit.controller.TerminalController;
import ch.highway2bit.entity.Kursrun;
import ch.highway2bit.model.ClassrunInfoOnDisplay;
import ch.highway2bit.service.KursRunService;
import ch.highway2bit.service.TerminalKursrunService;


@Component
public class CheckForNextClassrun {

	private static final Logger LOG = LoggerFactory.getLogger(CheckForNextClassrun.class);

	@Autowired
	private TerminalKursrunService kursrunService;

	@Autowired
	private ClassrunInfoOnDisplay classrunInfoOnDisplay;

	@Autowired
	private TerminalController terminaController;

	@Autowired
	private KursRunService kursRunService;

	@Scheduled(fixedRate = 5000)
	private void  checkNextClass(){
		LOG.info("Prüfe auf neunen Kurs: Im Moment läuft Kurs mit ID: {} auf dem Bildschirm.", classrunInfoOnDisplay.getId());

		// Welcher ist der nächste Kurs?
		Optional<Kursrun> nextClassrunAccordingDb = kursrunService.findNextKursrun();

		if(nextClassrunAccordingDb.isPresent()){
			LOG.info("Aktueller Kurs (APP scope)     mit Id: {}",classrunInfoOnDisplay.getId());
			LOG.info("Aktueller Kurs (Display View)  mit Id: {}",nextClassrunAccordingDb.get().getId());

			// Ist der neue Kurs dem Terminal bekannt?
			if(!nextClassrunAccordingDb.get().getId().equals(classrunInfoOnDisplay.getId())){

				// Id nachführen
				classrunInfoOnDisplay.setId(nextClassrunAccordingDb.get().getId());

				// Es gibt einen neuen Kurs! Frontend informieren!
				terminaController.sendNextClassRunInfo(nextClassrunAccordingDb.get());

			}else{
				LOG.info("Keine Änderungen");
			}
		}
		else {
			// Es gibt keinen Kurs im Moment. Dummy Kursrun laden
			if (classrunInfoOnDisplay.getId()!=null) {
				// Id nachführen --> Kein Kursrun
				classrunInfoOnDisplay.setId(null);

				// Informiere Frontend
				terminaController.sendNextClassRunInfo(kursRunService.getDummyClass());
			}
			LOG.info("Keine Änderungen");
		}
	}

}
