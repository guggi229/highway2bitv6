package ch.highway2bit.scheduletask;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ch.highway2bit.service.CheckpointTerminal;
import ch.highway2bit.service.TerminalService;
/**
 * Prüft periodisch den NFC Reader ob eine Karte hingehalten wurde.
 * @author guggisberste
 *
 */
@Component
public class NFCReaderTask {

	@Autowired
	private CheckpointTerminal checkpointTerminal;

	@Autowired
	private TerminalService terminalService;

	@Scheduled(fixedRate = 500)
	public void checkReader(){
		checkpointTerminal.readReceiver(terminalService);

	}

}
