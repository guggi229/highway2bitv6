package ch.highway2bit.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.highway2bit.controller.TerminalController;
import ch.highway2bit.exception.NfcCardNotFoundException;
import ch.highway2bit.exception.NoAbonnementFound;
import ch.highway2bit.exception.ThereIsNonClassrunNowException;
import ch.highway2bit.exception.UserIsInactiveException;
import ch.highway2bit.exception.UserNotFoundException;
import ch.highway2bit.terminal.TerminalTemplate;

@Service
public class CheckpointTerminal extends TerminalTemplate {


	// Text
	public static final String TEXT_NO_CLASS_NOW="Kein Kurs im Moment";

	@Autowired
	private TerminalController terminalController;


	@Override
	protected void terminalJob(byte[] answer) {
		try {
			terminalService.checkJob(bytesToHex(answer));
			terminalController.sendCustomerInfo(false);
		} catch (UserNotFoundException | UserIsInactiveException | NoAbonnementFound | NfcCardNotFoundException e) {
			terminalController.sendCustomerInfo("Bitte melde dich an der Theke", true);
		}
		catch (ThereIsNonClassrunNowException e) {
			terminalController.sendCustomerInfo("Du bist zu früh, bitte warte noch", true);
		}

	}

	@Override
	protected void cardIsRemoved() {
		// Nichts zu tun!

	}


}

